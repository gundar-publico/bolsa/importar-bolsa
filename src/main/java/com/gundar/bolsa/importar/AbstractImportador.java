package com.gundar.bolsa.importar;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.excepciones.RecordException;

/**
 * T es tipo de datos que se leerá: CsvRecord, JsonNode, etc
 * V es el tipo de datos que se devolverá para esta fuente
 */
public abstract class AbstractImportador<T, V extends FuenteDatosDia> {

  private static final Logger log = LoggerFactory.getLogger(AbstractImportador.class);

  private final String rutaBaseArchivos;

  public AbstractImportador(String rutaBaseArchivos) {
    super();
    this.rutaBaseArchivos = rutaBaseArchivos;
  }

  public List<V> importar() throws IOException {

    Map<LocalDate, V> valoresTotales = new HashMap<>();

    if (log.isInfoEnabled()) {
      log.info("Iniciando la carga = [" + getClass().getName() + "]");
    }

    Collection<String> nombreArchivos = getNombresArchivos();

    for (String nombreArchivo : nombreArchivos) {

      String rutaArchivo = rutaBaseArchivos + nombreArchivo;

      if (log.isInfoEnabled()) {
        log.info("Cargando el archivo = [" + rutaArchivo + "]");
      }

      Map<LocalDate, V> valoresArchivo = importar(rutaArchivo);

      if (log.isInfoEnabled()) {
        log.info("Cargados [" + valoresArchivo.size() + "] días con datos para el archivo = [" + rutaArchivo + "]");
      }

      anyadirValores(valoresTotales, valoresArchivo);
    }

    List<V> valoresOrdenados = ordenarInformacionPorDia(valoresTotales.values());

    if (log.isInfoEnabled()) {
      log.info("Cargados [" + valoresOrdenados.size() + "] días con datos en total para = [" + getClass().getName() + "]");
    }

    return valoresOrdenados;
  }

  protected abstract Collection<String> getNombresArchivos();

  private List<V> ordenarInformacionPorDia(Collection<V> valoresDesordenados) {

    List<V> valoresOrdenados = new ArrayList<>(valoresDesordenados);

    Collections.sort(valoresOrdenados);

    return valoresOrdenados;
  }

  private void anyadirValores(Map<LocalDate, V> valoresTotales, Map<LocalDate, V> valoresArchivo) {

    for (Map.Entry<LocalDate, V> valorArchivo : valoresArchivo.entrySet()) {

      if (valoresTotales.containsKey(valorArchivo.getKey())) {
        throw new IllegalStateException("Fecha repetida = [" + valorArchivo.getKey() + "]");
      }

      valoresTotales.put(valorArchivo.getKey(), valorArchivo.getValue());
    }

  }

  protected abstract Iterable<T> cargarRegistros(String rutaArchivo);

  private Map<LocalDate, V> importar(String rutaArchivo) throws IOException {

    Iterable<T> registros = cargarRegistros(rutaArchivo);

    Map<LocalDate, V> valores = new HashMap<>();

    int diasVacios = 0;

    for (T record : registros) {

      try {

        V fuenteDatosDia = convertirRegistro(record);

        final LocalDate fecha = fuenteDatosDia.getFecha();

        if (fuenteDatosDia.esVacio()) {

          if (log.isDebugEnabled()) {
            log.debug("Fecha vacía = [" + fecha + "]");
          }

          diasVacios++;

          continue;
        }

        if (valores.containsKey(fecha)) {
          throw new IllegalStateException("Fecha ya existente = [" + fecha + "]");
        }

        valores.put(fecha, fuenteDatosDia);
      } catch (RecordException e) {

        log.error("Error procesando el registro", e);
      }
    }

    if (log.isDebugEnabled()) {
      log.debug("Total dias vacios = [" + diasVacios + "]");
      log.debug("Total dias con datos = [" + valores.size() + "]");
      log.debug("Total dias = [" + (diasVacios + valores.size()) + "]");
    }


    return valores;
  }

  protected abstract V convertirRegistro(T record);

  protected String filtrarString(String valor) {

    if ((valor == null) || valor.isEmpty() || "null".equals(valor)) {
      valor = null;
    } else {
      valor = valor.trim();
    }

    return valor;
  }

}
