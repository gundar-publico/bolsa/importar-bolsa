package com.gundar.bolsa.importar;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface FuenteDatosDia extends Comparable<FuenteDatosDia> {

  public LocalDate getFecha();

  public BigDecimal getPrecioCierre();

  public BigDecimal getPrecioApertura();

  public Long getVolumen();

  public String getOrigen();

  public boolean esVacio();

}
