package com.gundar.bolsa.importar;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.excepciones.RecordException;

public abstract class AbstractImportadorCsv<V extends FuenteDatosDia> extends AbstractImportador<CSVRecord, V> {

  private final boolean primeraLineaCabecera;

  public AbstractImportadorCsv(String rutaBaseArchivos, boolean primeraLineaCabecera) {
    super(rutaBaseArchivos);
    this.primeraLineaCabecera = primeraLineaCabecera;
  }

  @Override
  protected V convertirRegistro(CSVRecord record) {

    if (!record.isConsistent()) {

      throw new RecordException("Estado inconsistente. Record = [" + record + "]");
    }

    return convertirRegistroCsv(record);
  }

  protected abstract V convertirRegistroCsv(CSVRecord record);

  @Override
  protected Iterable<CSVRecord> cargarRegistros(String rutaArchivo) {

    try {

      Reader in = new FileReader(rutaArchivo);

      CSVFormat parserCsv = CSVFormat.DEFAULT;

      if (primeraLineaCabecera) {
        parserCsv = parserCsv.withFirstRecordAsHeader();
      }

      Iterable<CSVRecord> records = parserCsv.parse(in);

      return records;

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
