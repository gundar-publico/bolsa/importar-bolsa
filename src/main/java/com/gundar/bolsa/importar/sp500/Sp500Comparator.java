package com.gundar.bolsa.importar.sp500;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.datos.CalculoDesviacion;
import com.gundar.bolsa.importar.datos.DesviacionPorcentualMaxBigDecimal;
import com.gundar.bolsa.importar.datos.DesviacionPorcentualMaxLong;
import com.gundar.bolsa.importar.datos.ExtractorPrecioCierre;
import com.gundar.bolsa.importar.datos.ExtractorVolumen;

public class Sp500Comparator extends Sp500Importador {

  private static final Logger log = LoggerFactory.getLogger(Sp500Comparator.class);

  public static void main(String[] args) throws Exception {

    new Sp500Comparator().compararPrecios();
    // new Sp500Comparator().compararVolumen();
  }

  public void compararPrecios() throws IOException {

    Map<LocalDate, Collection<FuenteDatosSp500>> fuentesAgrupadas = cargarAgruparPorDias();

    CalculoDesviacion desviacion = new DesviacionPorcentualMaxBigDecimal(0.1, ExtractorPrecioCierre.INSTANCIA);
    // CalculoDesviacion desviacion = new DesviacionLinealMinBigDecimal(0, ExtractorPrecioCierre.INSTANCIA);

    int distintos = 0;
    for (Map.Entry<LocalDate, Collection<FuenteDatosSp500>> diaConFuente : fuentesAgrupadas.entrySet()) {

      boolean desviado = desviacion.valoresDesviados(diaConFuente.getValue());
      if (desviado) {
        distintos++;
      }

    }

    if (log.isWarnEnabled()) {
      log.warn("Total errores = [" + distintos + "]");
    }
  }

  public void compararVolumen() throws IOException {

    Map<LocalDate, Collection<FuenteDatosSp500>> fuentesAgrupadas = cargarAgruparPorDias();

    // CalculoDesviacion desviacion = new DesviacionPorcentualMin(1, ExtractorVolumen.INSTANCIA);
    CalculoDesviacion desviacion = new DesviacionPorcentualMaxLong(1, ExtractorVolumen.INSTANCIA);
    // CalculoDesviacion desviacion = new DesviacionLinealMax(1);
    // CalculoDesviacion desviacion = new DesviacionLinealMin(5);

    int distintos = 0;
    for (Map.Entry<LocalDate, Collection<FuenteDatosSp500>> diaConFuente : fuentesAgrupadas.entrySet()) {

      boolean desviado = desviacion.valoresDesviados(diaConFuente.getValue());
      if (desviado) {
        distintos++;
      }

    }

    if (log.isWarnEnabled()) {
      log.warn("Total errores = [" + distintos + "]");
    }
  }

}
