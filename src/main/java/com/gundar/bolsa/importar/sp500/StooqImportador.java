package com.gundar.bolsa.importar.sp500;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;

class StooqImportador extends AbstractImportadorCsv<FuenteDatosSp500> {

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_OPEN = 1;
  private static final int CABECERA_HIGH = 2;
  private static final int CABECERA_LOW = 3;
  private static final int CABECERA_CLOSE = 4;
  private static final int CABECERA_VOLUME = 5;

  public StooqImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new StooqImportador(Sp500Importador.RUTA_BASE_SP500).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Collections.singletonList("stooq_spx_d.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  @Override
  protected FuenteDatosSp500 convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    LocalDate fecha = LocalDate.parse(dateCsv, FORMATO_FECHA);

    String openCsv = filtrarString(record.get(CABECERA_OPEN));

    String highCsv = filtrarString(record.get(CABECERA_HIGH));

    String lowCsv = filtrarString(record.get(CABECERA_LOW));

    String closeCsv = filtrarString(record.get(CABECERA_CLOSE));

    String volumeCsv = filtrarString(record.get(CABECERA_VOLUME));

    final FuenteDatosSp500 fuenteDatosDia;

    if (closeCsv == null) {

      if ((openCsv != null) || (highCsv != null) || (lowCsv != null) || (volumeCsv != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDatosDia = new FuenteDatosSp500(fecha);

    } else {

      BigDecimal valorCierre = new BigDecimal(closeCsv);

      Long volume = null;
      if ((volumeCsv != null) && !"0".equals(volumeCsv)) {
        volume = Long.valueOf(volumeCsv);
      }

      fuenteDatosDia = new FuenteDatosSp500(fecha, valorCierre, volume, "Stooq");
    }

    return fuenteDatosDia;
  }

}
