package com.gundar.bolsa.importar.sp500.preprocesor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.parser.ImportadorHttpAbstract;
import com.gundar.bolsa.importar.parser.google.GoogleFinanceParser;
import com.gundar.bolsa.importar.sp500.Sp500Importador;

class GoogleFinanceSp500 extends ImportadorHttpAbstract {

  private static final Logger log = LoggerFactory.getLogger(GoogleFinanceSp500.class);

  public static final LocalDate FECHA_INICIAL_INDICE = LocalDate.of(1970, 1, 1);
  private static final String CODIGO_SP500 = "626307";

  public GoogleFinanceSp500() {
    super(new GoogleFinanceParser(CODIGO_SP500));
  }

  public static void main(String[] args) throws Exception {

    LocalDate fechaFinal = LocalDate.of(2018, 12, 31);

    new GoogleFinanceSp500().importarDatosParaArchivo(FECHA_INICIAL_INDICE, fechaFinal);
  }

  @Override
  protected void escribirResultados(List<String> lineasTransformadas) {

    try {

      String rutaArchivo = Sp500Importador.RUTA_BASE_SP500 + "google.csv";

      Files.write(Paths.get(rutaArchivo), lineasTransformadas, StandardOpenOption.CREATE, StandardOpenOption.WRITE);

      if (log.isInfoEnabled()) {
        log.info("Creado archivo = [" + rutaArchivo + "]");
      }

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
