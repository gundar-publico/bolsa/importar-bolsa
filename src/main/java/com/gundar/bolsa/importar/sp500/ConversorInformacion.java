// package com.gundar.bolsa.importar.sp500;
//
// import java.math.BigDecimal;
// import java.math.MathContext;
// import java.time.LocalDate;
// import java.util.Collection;
//
// import com.gundar.bolsa.InformacionDia;
// import com.gundar.bolsa.datos.SeleccionadorPrecio;
// import com.gundar.bolsa.importar.FuenteDatosDia;
//
// public class ConversorInformacion {
//
// // public static final long PRECIO_VACIO = -1;
//
// private final SeleccionadorPrecio seleccionadorPrecio;
//
// public ConversorInformacion(SeleccionadorPrecio seleccionadorPrecio) {
// super();
// this.seleccionadorPrecio = seleccionadorPrecio;
// }
//
// public InformacionDia convertir(BigDecimal precioAnterior, Collection<? extends FuenteDatosDia> fuentesDia) {
//
// LocalDate fecha = fuentesDia.iterator().next().getFecha();
//
// BigDecimal precio = seleccionadorPrecio.seleccionar(fuentesDia);
//
// final double diferenciaPorcentual;
// final BigDecimal diferenciaLineal;
//
// if (precioAnterior == null) {
// diferenciaLineal = BigDecimal.ZERO;
// diferenciaPorcentual = 0;
// } else {
// diferenciaLineal = calculaDiferenciaLineal(precio, precioAnterior);
// diferenciaPorcentual = calculaDiferenciaPorcentual(diferenciaLineal, precioAnterior);
// }
//
// return new InformacionDia(fecha, precio, diferenciaPorcentual, diferenciaLineal);
// }
//
// private BigDecimal calculaDiferenciaLineal(BigDecimal precioActual, BigDecimal precioAnterior) {
//
// return precioActual.subtract(precioAnterior);
// }
//
// private double calculaDiferenciaPorcentual(BigDecimal diferenciaLineal, BigDecimal precioAnterior) {
//
// // BigDecimal diferencia = precioActual.subtract(precioAnterior);
//
// // long diferencia = precioActual - precioAnterior;
//
// // double diferenciaPorcentual = (double) diferencia / precioAnterior;
//
// BigDecimal diferenciaPorcentual = diferenciaLineal.divide(precioAnterior, MathContext.DECIMAL128);
//
// return diferenciaPorcentual.doubleValue();
// }
//
// }
