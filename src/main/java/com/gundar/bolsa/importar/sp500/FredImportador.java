package com.gundar.bolsa.importar.sp500;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;

class FredImportador extends AbstractImportadorCsv<FuenteDatosSp500> {

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_CLOSE = 1;

  public FredImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new FredImportador(Sp500Importador.RUTA_BASE_SP500).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Collections.singletonList("fred_SP500.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  @Override
  protected FuenteDatosSp500 convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    LocalDate fecha = LocalDate.parse(dateCsv, FORMATO_FECHA);

    String closeCsv = filtrarString(record.get(CABECERA_CLOSE));

    final FuenteDatosSp500 fuenteDatosDia;

    if ((closeCsv == null) || esVacio(closeCsv)) {

      fuenteDatosDia = new FuenteDatosSp500(fecha);

    } else {

      BigDecimal valorCierre = new BigDecimal(closeCsv);

      final Long volume = null;

      fuenteDatosDia = new FuenteDatosSp500(fecha, valorCierre, volume, "Fred");
    }

    return fuenteDatosDia;
  }

  private boolean esVacio(String valorString) {

    final String CAMPO_VACIO = ".";

    return CAMPO_VACIO.equals(valorString);
  }

}
