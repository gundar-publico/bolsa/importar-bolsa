package com.gundar.bolsa.importar.sp500;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;

class DailyPriceHistoryImportador extends AbstractImportadorCsv<FuenteDatosSp500> {

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_CLOSE = 1;

  public DailyPriceHistoryImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, false);
  }

  public static void main(String[] args) throws Exception {

    new DailyPriceHistoryImportador(Sp500Importador.RUTA_BASE_SP500).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Collections.singletonList("dailypricehistory.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("dd/MM/yyyy");

  @Override
  protected FuenteDatosSp500 convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    // LocalDate fecha = parseDate(dateCsv);
    LocalDate fecha = LocalDate.parse(dateCsv, FORMATO_FECHA);

    String closeCsv = filtrarString(record.get(CABECERA_CLOSE));

    final FuenteDatosSp500 fuenteDatosDia;

    if (closeCsv == null) {

      fuenteDatosDia = new FuenteDatosSp500(fecha);

    } else {

      closeCsv = closeCsv.replace(',', '.');
      BigDecimal valorCierre = new BigDecimal(closeCsv);

      final Long volume = null;

      fuenteDatosDia = new FuenteDatosSp500(fecha, valorCierre, volume, "DailyPriceHistory");
    }

    return fuenteDatosDia;
  }

}
