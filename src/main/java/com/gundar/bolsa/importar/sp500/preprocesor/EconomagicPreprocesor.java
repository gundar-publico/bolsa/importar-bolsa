package com.gundar.bolsa.importar.sp500.preprocesor;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

class EconomagicPreprocesor {

  public static void main(String[] args) throws Exception {

    new EconomagicPreprocesor().transformarArchivo();
  }

  public void transformarArchivo() throws IOException {

    List<String> lineasOriginales = leerArchivoOriginal();

    BigDecimal precioAnterior = new BigDecimal("16.66");// Ponemos a mano el primer precio conocido
    List<String> lineasTransformadas = new ArrayList<>(lineasOriginales.size());

    for (String lineaOriginal : lineasOriginales) {

      if (lineaOriginal.isEmpty()) {
        continue;
      }

      LineaTransformada lineaTransformada = transformarLinea(lineaOriginal, precioAnterior);

      String lineaTransformadaString = lineaTransformada.crearLinea();

      lineasTransformadas.add(lineaTransformadaString);

      precioAnterior = lineaTransformada.precio;
    }

    escribirArchivoTransformado(lineasTransformadas);
  }

  private List<String> leerArchivoOriginal() throws IOException {

    String rutaArchivo = "/home/gun/workspace_oxygen/importar-bolsa/src/main/resources/fuentes/sp500/economagic.txt";

    List<String> lineas = Files.readAllLines(Paths.get(rutaArchivo));

    return lineas;
  }

  private void escribirArchivoTransformado(List<String> lineasTransformadas) throws IOException {

    String rutaArchivo = "/home/gun/workspace_oxygen/importar-bolsa/src/main/resources/fuentes/sp500/economagic.csv";

    Files.write(Paths.get(rutaArchivo), lineasTransformadas, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
  }

  private static final Pattern SEPARADOR_LINEA_ORIGINAL = Pattern.compile(",");

  private LineaTransformada transformarLinea(String lineaOriginal, BigDecimal precioAnterior) {

    String campos[] = SEPARADOR_LINEA_ORIGINAL.split(lineaOriginal);

    if (campos.length != 4) {
      throw new IllegalStateException("Se esperaban 4 campos y se han recibido = [" + campos.length + "]. Linea = [" + lineaOriginal + "]");
    }

    String anyoString = campos[0];
    String mesString = campos[1];
    String diaString = campos[2];
    String precioString = campos[3];

    int anyo = Integer.parseInt(anyoString);
    int mes = Integer.parseInt(mesString);
    int dia = Integer.parseInt(diaString);

    LocalDate fecha = LocalDate.of(anyo, mes, dia);

    BigDecimal precio = transformarPrecio(precioString, precioAnterior);

    return new LineaTransformada(fecha, precio);
  }

  private static final double MAXIMA_DIFERENCIA_PORCENTUAL = 50;
  private static final String SEPARADOR_DECIMALES = ".";

  private BigDecimal transformarPrecio(String precioString, BigDecimal precioAnterior) {

    /* Cambiamos los separadores de decimales "extraños" por el estandar */
    precioString = precioString.replace("·", SEPARADOR_DECIMALES);

    double diferenciaPorcentual = Double.MAX_VALUE;

    BigDecimal precio = BigDecimal.valueOf(-1);

    /* Si la diferencia es exagerada es que aun estamos con valores falsos */
    while (diferenciaPorcentual > MAXIMA_DIFERENCIA_PORCENTUAL) {

      precio = new BigDecimal(precioString);

      diferenciaPorcentual = calculaDiferenciaPorcentual(precio, precioAnterior);

      /* Vamos quitando un caracter de cada vez hasta llegar al valor bueno */
      precioString = precioString.substring(1);
    }

    return precio;
  }

  private double calculaDiferenciaPorcentual(BigDecimal precio, BigDecimal precioAnterior) {

    double diferencia = Math.abs(precio.doubleValue() - precioAnterior.doubleValue());

    double proporcion = (diferencia / precioAnterior.doubleValue()) * 100;

    return proporcion;
  }

  private static class LineaTransformada {

    private LocalDate fecha;
    private BigDecimal precio;

    public LineaTransformada(LocalDate fecha, BigDecimal precio) {
      super();
      this.fecha = fecha;
      this.precio = precio;
    }

    public String crearLinea() {

      return fecha + "," + precio;
    }

  }

}
