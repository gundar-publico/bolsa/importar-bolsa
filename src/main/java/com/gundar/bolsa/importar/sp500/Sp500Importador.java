package com.gundar.bolsa.importar.sp500;

import static com.gundar.bolsa.importar.ImportadorComun.calculaDiferenciaLineal;
import static com.gundar.bolsa.importar.ImportadorComun.calculaDiferenciaPorcentual;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.ImportadorIndice;
import com.gundar.bolsa.importar.InformacionDia;
import com.gundar.bolsa.importar.datos.ExtractorPrecioCierre;
import com.gundar.bolsa.importar.datos.SeleccionadorPrecio;

public class Sp500Importador implements ImportadorIndice {

  private static final Logger log = LoggerFactory.getLogger(Sp500Importador.class);

  public static final String RUTA_BASE_SP500 = "/home/gun/workspace_oxygen/importar-bolsa/src/main/resources/fuentes/sp500/";
  protected static final LocalDate FECHA_MAXIMA = LocalDate.of(2017, 12, 31);

  public static void main(String[] args) throws Exception {

    new Sp500Importador().importarInformacionDia();
  }

  @Override
  public List<InformacionDia> importarInformacionDia() throws IOException {

    Map<LocalDate, Collection<FuenteDatosSp500>> fuentesAgrupadas = cargarAgruparPorDias();

    // ConversorInformacion conversor = new ConversorInformacion(new SeleccionadorPrecio(0.75));

    SeleccionadorPrecio seleccionadorPrecioCierre = new SeleccionadorPrecio(0.75, ExtractorPrecioCierre.INSTANCIA);

    List<InformacionDia> informacionTotal = new ArrayList<>();

    // long precioAnterior = ConversorInformacion.PRECIO_VACIO;
    BigDecimal precioCierreAnterior = null;

    for (Map.Entry<LocalDate, Collection<FuenteDatosSp500>> diaConFuentes : fuentesAgrupadas.entrySet()) {

      LocalDate fecha = diaConFuentes.getKey();

      if (fecha.compareTo(FECHA_MAXIMA) > 0) {

        if (log.isDebugEnabled()) {

          log.debug("La fecha = [" + fecha + "] supera la fecha máxima = [" + FECHA_MAXIMA + "]");
        }

        continue;
      }

      BigDecimal precioCierre = seleccionadorPrecioCierre.seleccionar(diaConFuentes.getValue());

      final double diferenciaPorcentual;
      final BigDecimal diferenciaLineal;

      if (precioCierreAnterior == null) {
        diferenciaLineal = BigDecimal.ZERO;
        diferenciaPorcentual = 0;
      } else {
        diferenciaLineal = calculaDiferenciaLineal(precioCierre, precioCierreAnterior);
        diferenciaPorcentual = calculaDiferenciaPorcentual(diferenciaLineal.doubleValue(), precioCierreAnterior.doubleValue());
      }

      final BigDecimal precioApertura = null;
      // InformacionDia informacionDia = conversor.convertir(precioCierreAnterior, diaConFuentes.getValue());
      InformacionDia informacionDia = new InformacionDia(fecha, precioCierre, precioApertura, diferenciaPorcentual, diferenciaLineal);
      informacionTotal.add(informacionDia);

      precioCierreAnterior = informacionDia.getPrecioCierre();
    }

    return informacionTotal;
  }

  protected Map<LocalDate, Collection<FuenteDatosSp500>> cargarAgruparPorDias() throws IOException {

    List<FuenteDatosSp500> valoresDailyPriceHistory = new DailyPriceHistoryImportador(RUTA_BASE_SP500).importar();

    List<FuenteDatosSp500> valoresEconomagic = new EconomagicImportador(RUTA_BASE_SP500).importar();

    List<FuenteDatosSp500> valoresFred = new FredImportador(RUTA_BASE_SP500).importar();

    List<FuenteDatosSp500> valoresGoogle = new GoogleImportador(RUTA_BASE_SP500).importar();

    List<FuenteDatosSp500> valoresStooq = new StooqImportador(RUTA_BASE_SP500).importar();

    List<FuenteDatosSp500> valoresWallStreet = new WallStreetImportador(RUTA_BASE_SP500).importar();

    List<FuenteDatosSp500> valoresYahoo = new YahooImportador(RUTA_BASE_SP500).importar();

    Map<LocalDate, Collection<FuenteDatosSp500>> fuentesAgrupadas = agrupar(
        valoresDailyPriceHistory,
        valoresEconomagic,
        valoresWallStreet,
        valoresYahoo,
        valoresFred,
        valoresGoogle,
        valoresStooq);

    return fuentesAgrupadas;
  }

  @SafeVarargs
  private final Map<LocalDate, Collection<FuenteDatosSp500>> agrupar(Collection<FuenteDatosSp500>... todosLosValores) {

    Map<LocalDate, Collection<FuenteDatosSp500>> informacionPorDia = new TreeMap<>();

    for (Collection<FuenteDatosSp500> valores : todosLosValores) {

      for (FuenteDatosSp500 valorDia : valores) {

        Collection<FuenteDatosSp500> valoresAgrupados = informacionPorDia.get(valorDia.getFecha());

        if (valoresAgrupados == null) {
          valoresAgrupados = new ArrayList<>(todosLosValores.length);
          informacionPorDia.put(valorDia.getFecha(), valoresAgrupados);
        }

        valoresAgrupados.add(valorDia);
      }
    }

    return informacionPorDia;
  }

}
