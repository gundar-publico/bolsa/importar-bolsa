package com.gundar.bolsa.importar.sp500;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;

class WallStreetImportador extends AbstractImportadorCsv<FuenteDatosSp500> {

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_OPEN = 1;
  private static final int CABECERA_HIGH = 2;
  private static final int CABECERA_LOW = 3;
  private static final int CABECERA_CLOSE = 4;

  public WallStreetImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new WallStreetImportador(Sp500Importador.RUTA_BASE_SP500).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {
    return Collections.singletonList("wall_street.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("MM/dd/yyyy");

  @Override
  protected FuenteDatosSp500 convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    LocalDate fecha = LocalDate.parse(dateCsv, FORMATO_FECHA);

    String openCsv = filtrarString(record.get(CABECERA_OPEN));

    String highCsv = filtrarString(record.get(CABECERA_HIGH));

    String lowCsv = filtrarString(record.get(CABECERA_LOW));

    String closeCsv = filtrarString(record.get(CABECERA_CLOSE));

    final FuenteDatosSp500 fuenteDiaIbex35;

    if (closeCsv == null) {

      if ((openCsv != null) || (highCsv != null) || (lowCsv != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDiaIbex35 = new FuenteDatosSp500(fecha);

    } else {

      BigDecimal valorCierre = new BigDecimal(closeCsv);

      final Long volumen = null;

      fuenteDiaIbex35 = new FuenteDatosSp500(fecha, valorCierre, volumen, "Wall Street");
    }

    return fuenteDiaIbex35;
  }

}
