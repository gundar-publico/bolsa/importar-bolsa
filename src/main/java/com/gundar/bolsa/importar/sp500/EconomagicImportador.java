package com.gundar.bolsa.importar.sp500;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;

class EconomagicImportador extends AbstractImportadorCsv<FuenteDatosSp500> {

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_CLOSE = 1;

  public EconomagicImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, false);
  }

  public static void main(String[] args) throws Exception {

    new EconomagicImportador(Sp500Importador.RUTA_BASE_SP500).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Collections.singletonList("economagic.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  @Override
  protected FuenteDatosSp500 convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    LocalDate fecha = LocalDate.parse(dateCsv, FORMATO_FECHA);

    String closeCsv = filtrarString(record.get(CABECERA_CLOSE));

    final FuenteDatosSp500 fuenteDatosDia;

    if (closeCsv == null) {

      fuenteDatosDia = new FuenteDatosSp500(fecha);

    } else {

      BigDecimal valorCierre = new BigDecimal(closeCsv);

      final Long volume = null;

      fuenteDatosDia = new FuenteDatosSp500(fecha, valorCierre, volume, "Economagic");
    }

    return fuenteDatosDia;
  }

}
