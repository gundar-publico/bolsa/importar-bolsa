package com.gundar.bolsa.importar.excepciones;


public class RecordException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public RecordException(String message, Throwable cause) {
    super(message, cause);
  }

  public RecordException(String message) {
    super(message);
  }

  public RecordException(Throwable cause) {
    super(cause);
  }

}
