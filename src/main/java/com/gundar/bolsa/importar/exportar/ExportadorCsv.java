package com.gundar.bolsa.importar.exportar;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.ImportadorIndice;
import com.gundar.bolsa.importar.InformacionDia;
import com.gundar.bolsa.importar.sp500.Sp500Importador;

public class ExportadorCsv {

  private static final String RUTA_BASE = "/home/gun/workspace_oxygen/importar-bolsa/exportar/";
  private static final Logger log = LoggerFactory.getLogger(ExportadorCsv.class);
  private static final int LOTE = 500;

  public static void main(String[] args) throws Exception {

    // new ExportadorCsv().exportar(new Ibex35Importador(), "ibex35.csv");
    new ExportadorCsv().exportar(new Sp500Importador(), "sp500.csv");
    // new ExportadorCsv().exportar(new Dax30Importador(), "dax30.csv");
    // new ExportadorCsv().exportar(new LyxorFR0011036268(), "FR0011036268.csv");
    // new ExportadorCsv().exportar(new LyxorFR0011042753(), "FR0011042753.csv");
    // new ExportadorCsv().exportar(new LyxorFR0010762492(), "FR0010762492.csv");
  }

  public void exportar(ImportadorIndice importadorIndice, String nombreDestino) throws IOException {

    List<InformacionDia> informacionDias = importadorIndice.importarInformacionDia();

    escribirResultados(informacionDias, nombreDestino);
  }

  private void escribirResultados(List<InformacionDia> informacionDias, String nombreDestino) throws IOException {

    Path rutaArchivo = Paths.get(RUTA_BASE, nombreDestino);

    if (log.isInfoEnabled()) {
      log.info("Escribiendo resultados al archivo = [" + rutaArchivo + "]");
    }

    int iteracionesTotales = 0;

    try (
        CSVPrinter printer = CSVFormat.DEFAULT
            .withHeader("fecha", "precio", "precio apt", "dif", "dif.%")
            .print(rutaArchivo, StandardCharsets.UTF_8)) {

      int iteracionLote = 0;
      for (InformacionDia informacionDia : informacionDias) {

        printer.printRecord(
            informacionDia.getFecha(),
            informacionDia.getPrecioCierre(),
            informacionDia.getPrecioApertura(),
            informacionDia.getDiferenciaLineal(),
            informacionDia.getDiferenciaPorcentual());

        iteracionesTotales++;

        if (iteracionLote++ > LOTE) {
          iteracionLote = 0;
          printer.flush();
        }
      }
    }

    if (log.isInfoEnabled()) {
      log.info("Finalizado con [" + iteracionesTotales + "] lineas el archivo = [" + rutaArchivo + "]");
    }
  }

}
