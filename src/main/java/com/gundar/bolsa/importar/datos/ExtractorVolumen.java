package com.gundar.bolsa.importar.datos;

import com.gundar.bolsa.importar.FuenteDatosDia;

public class ExtractorVolumen implements ExtractorCampo<Long> {

  public static final ExtractorVolumen INSTANCIA = new ExtractorVolumen();

  @Override
  public Long extraer(FuenteDatosDia fuenteDatosDia) {

    return fuenteDatosDia.getVolumen();
  }

}
