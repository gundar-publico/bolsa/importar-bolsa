package com.gundar.bolsa.importar.datos;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.FuenteDatosDia;

public class SeleccionadorPrecio {

  private static final Logger log = LoggerFactory.getLogger(SeleccionadorPrecio.class);

  private final double quorumMinimoDeseable;
  private final ExtractorCampo<BigDecimal> extractorPrecio;

  public SeleccionadorPrecio(double quorumMinimoDeseable, ExtractorCampo<BigDecimal> extractorPrecio) {
    super();
    this.quorumMinimoDeseable = quorumMinimoDeseable;
    this.extractorPrecio = extractorPrecio;
  }

  public BigDecimal seleccionar(Collection<? extends FuenteDatosDia> fuentesDia) {

    LocalDate fecha = fuentesDia.iterator().next().getFecha();

    Map<BigDecimal, Collection<String>> origenesPorValor = agruparOrigenesPorValor(fuentesDia);

    int numeroPreciosDistintos = origenesPorValor.size();

    final BigDecimal precioSeleccionado;

    if (numeroPreciosDistintos == 1) {

      precioSeleccionado = origenesPorValor.keySet().iterator().next();

      if (log.isWarnEnabled()) {

        if (fuentesDia.size() == 1) {
          String origen = origenesPorValor.values().iterator().next().iterator().next();
          log.warn("Precio seleccionado de un solo origen = [" + origen + "] para la fecha = [" + fecha + "]");
        }
      }

    } else {

      precioSeleccionado = calcularPrecioMasComun(origenesPorValor, fecha);
    }

    return precioSeleccionado;
  }

  private BigDecimal calcularPrecioMasComun(Map<BigDecimal, Collection<String>> origenesPorValor, LocalDate fecha) {

    Entry<BigDecimal, Collection<String>> mejorValorConOrigen = new Entry<BigDecimal, Collection<String>>() {

      @Override
      public BigDecimal getKey() {
        return null;
      }

      @Override
      public Collection<String> getValue() {
        return Collections.emptyList();
      }

      @Override
      public Collection<String> setValue(Collection<String> value) {
        throw new UnsupportedOperationException();
      }
    };

    for (Entry<BigDecimal, Collection<String>> valorConOrigen : origenesPorValor.entrySet()) {

      if (mejorValorConOrigen == null) {

        mejorValorConOrigen = valorConOrigen;

      } else {

        int numeroOrigenesMejorPrecio = mejorValorConOrigen.getValue().size();

        int numeroOrigenesPrecioActual = valorConOrigen.getValue().size();

        if (numeroOrigenesPrecioActual > numeroOrigenesMejorPrecio) {

          mejorValorConOrigen = valorConOrigen;
        }

      }
    }

    if (log.isWarnEnabled()) {

      int numeroOrigenesMejorPrecio = mejorValorConOrigen.getValue().size();

      int numeroPreciosDistintos = origenesPorValor.size();

      double proporcionMejorPrecio = (double) numeroOrigenesMejorPrecio / numeroPreciosDistintos;

      if (Double.compare(proporcionMejorPrecio, quorumMinimoDeseable) < 0) {

        log.warn("Precio seleccionado con un quorum = ["
            + proporcionMejorPrecio + "] menor que el deseable = [" + quorumMinimoDeseable + "] para la fecha = [" + fecha + "]");
      }

    }

    return mejorValorConOrigen.getKey();
  }

  private Map<BigDecimal, Collection<String>> agruparOrigenesPorValor(Collection<? extends FuenteDatosDia> fuentesDia) {

    Map<BigDecimal, Collection<String>> origenesPorValor = new TreeMap<>();

    for (FuenteDatosDia infoDia : fuentesDia) {

      BigDecimal precio = extractorPrecio.extraer(infoDia);

      if (precio == null) {
        continue;
      }

      Collection<String> origenesIguales = origenesPorValor.get(precio);

      if (origenesIguales == null) {
        origenesIguales = new ArrayList<>();
        origenesPorValor.put(precio, origenesIguales);
      }

      origenesIguales.add(infoDia.getOrigen());
    }

    return origenesPorValor;
  }

}
