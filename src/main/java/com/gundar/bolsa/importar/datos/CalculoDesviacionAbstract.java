package com.gundar.bolsa.importar.datos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.FuenteDatosDia;

public abstract class CalculoDesviacionAbstract<T> implements CalculoDesviacion {

  private static final Logger log = LoggerFactory.getLogger(CalculoDesviacionAbstract.class);

  private final ExtractorCampo<T> extractorCampo;

  public CalculoDesviacionAbstract(ExtractorCampo<T> extractorCampo) {
    super();
    this.extractorCampo = extractorCampo;
  }

  @Override
  public boolean valoresDesviados(Collection<? extends FuenteDatosDia> valoresFuentes) {

    Map<T, Collection<String>> origenesPorValor = agruparOrigenesPorValor(valoresFuentes);

    LocalDate fecha = valoresFuentes.iterator().next().getFecha();

    boolean distintos = false;

    int numeroValoresDistintos = origenesPorValor.size();

    if (numeroValoresDistintos == 0) {

      if (log.isWarnEnabled()) {
        log.warn("Sin valores para la fecha = [" + fecha + "]");
      }

    } else if (origenesPorValor.size() > 1) {

      Set<T> valoresDistintos = origenesPorValor.keySet();

      if (superaDesviacionMaxima(valoresDistintos)) {

        distintos = true;

        StringBuilder errorBuilder = new StringBuilder("Valores distintos para la fecha = [" + fecha + "]");

        List<Map.Entry<T, Collection<String>>> diferenciasOrdenadas = ordenarDiferencias(origenesPorValor);

        for (Map.Entry<T, Collection<String>> origenValor : diferenciasOrdenadas) {

          String origenesParaEsteValor = origenValor.getValue().stream().collect(Collectors.joining(", ", "[", "]"));
          errorBuilder.append(" ").append(origenesParaEsteValor).append(" = [").append(origenValor.getKey()).append("]");
        }

        log.error(errorBuilder.toString());
      }
    }

    return distintos;
  }

  protected abstract boolean superaDesviacionMaxima(Collection<T> valores);

  private Map<T, Collection<String>> agruparOrigenesPorValor(Collection<? extends FuenteDatosDia> valoresFuentes) {

    Map<T, Collection<String>> origenesPorValor = new TreeMap<>();

    for (FuenteDatosDia infoDia : valoresFuentes) {

      T valorAgrupacion = extractorCampo.extraer(infoDia);

      if (valorAgrupacion == null) {
        continue;
      }

      // Collection<String> origenesIguales = origenesPorValor.get(infoDia.getValorCierre());
      Collection<String> origenesIguales = origenesPorValor.get(valorAgrupacion);

      if (origenesIguales == null) {
        origenesIguales = new ArrayList<>();
        origenesPorValor.put(valorAgrupacion, origenesIguales);
      }

      origenesIguales.add(infoDia.getOrigen());
    }

    return origenesPorValor;
  }

  private List<Map.Entry<T, Collection<String>>> ordenarDiferencias(Map<T, Collection<String>> diferenciasDesordenadas) {

    List<Map.Entry<T, Collection<String>>> diferenciasOrdenadas = new ArrayList<>(diferenciasDesordenadas.entrySet());

    Collections.sort(diferenciasOrdenadas, new ComparadorDiferencias<T>());

    return diferenciasOrdenadas;
  }

  private static class ComparadorDiferencias<T> implements Comparator<Map.Entry<T, Collection<String>>> {

    @Override
    public int compare(Map.Entry<T, Collection<String>> m1, Map.Entry<T, Collection<String>> m2) {

      Collection<String> o1 = m1.getValue();

      Collection<String> o2 = m2.getValue();

      /* Ordenamos de mayor a menor tamañano */
      int comparacionTamayo = Integer.compare(o2.size(), o1.size());

      if (comparacionTamayo != 0) {
        return comparacionTamayo;
      }

      if ((o1.size() == 1) && (o2.size() == 1)) {

        return o1.iterator().next().compareTo(o2.iterator().next());
      }

      return 0;
    }

  }

}
