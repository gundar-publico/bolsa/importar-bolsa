package com.gundar.bolsa.importar.datos;

import java.util.Collection;

import com.gundar.bolsa.importar.FuenteDatosDia;

public interface CalculoDesviacion {

  public boolean valoresDesviados(Collection<? extends FuenteDatosDia> valoresFuentes);

}
