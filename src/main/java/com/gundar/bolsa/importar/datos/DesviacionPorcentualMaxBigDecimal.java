package com.gundar.bolsa.importar.datos;

import java.math.BigDecimal;

public class DesviacionPorcentualMaxBigDecimal extends DesviacionPorcentualMax<BigDecimal> {

  public DesviacionPorcentualMaxBigDecimal(double desviacionMaximaPermitida, ExtractorCampo<BigDecimal> extractorCampo) {
    super(desviacionMaximaPermitida, extractorCampo);
  }

  @Override
  protected double calculaMaximaDesviacionPorcentual(BigDecimal valorAnterior, BigDecimal valorPosterior,
      double maximaDesviacionPorcentual) {

    BigDecimal diferencia = valorAnterior.subtract(valorPosterior).abs();

    double desviacionActualPorcentual = tantoPorCiento(diferencia, valorAnterior);

    maximaDesviacionPorcentual = Math.max(maximaDesviacionPorcentual, desviacionActualPorcentual);

    return maximaDesviacionPorcentual;
  }

  private double tantoPorCiento(BigDecimal parcial, BigDecimal total) {

    return (parcial.doubleValue() * 100.00) / total.doubleValue();
  }

}
