package com.gundar.bolsa.importar.datos;


public class DesviacionPorcentualMinLong extends DesviacionPorcentualMin<Long> {

  public DesviacionPorcentualMinLong(double desviacionMaximaPermitida, ExtractorCampo<Long> extractorCampo) {
    super(desviacionMaximaPermitida, extractorCampo);
  }

  @Override
  protected double calculaMinimaDesviacionPorcentual(Long valorAnterior, Long valorPosterior, double minimaDesviacionPorcentual) {

    long diferencia = Math.abs(valorAnterior - valorPosterior);

    double desviacionActualPorcentual = tantoPorCiento(diferencia, valorAnterior);

    minimaDesviacionPorcentual = Math.min(minimaDesviacionPorcentual, desviacionActualPorcentual);
    return minimaDesviacionPorcentual;
  }

  private double tantoPorCiento(long parcial, long total) {

    return (parcial * 100.00) / total;
  }

}
