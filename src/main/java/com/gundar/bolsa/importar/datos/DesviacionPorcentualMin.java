package com.gundar.bolsa.importar.datos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class DesviacionPorcentualMin<T> extends CalculoDesviacionAbstract<T> {

  private final double desviacionMaximaPermitida;

  public DesviacionPorcentualMin(double desviacionMaximaPermitida, ExtractorCampo<T> extractorCampo) {
    super(extractorCampo);
    this.desviacionMaximaPermitida = desviacionMaximaPermitida;
  }

  @Override
  protected boolean superaDesviacionMaxima(Collection<T> valores) {

    double menorDesviacion = buscaMenorDesviacion(valores);

    return Double.compare(menorDesviacion, desviacionMaximaPermitida) > 0;
  }

  private double buscaMenorDesviacion(Collection<T> valores) {

    List<T> valoresLista = new ArrayList<>(valores);

    double minimaDesviacionPorcentual = Double.MAX_VALUE;

    for (int i = 0; i < valoresLista.size(); i++) {

      T valorAnterior = valoresLista.get(i);

      for (int j = i + 1; j < valoresLista.size(); j++) {

        T valorPosterior = valoresLista.get(j);

        // long diferencia = Math.abs(valorAnterior - valorPosterior);
        //
        // double desviacionActualPorcentual = tantoPorCiento(diferencia, valorAnterior);
        //
        // minimaDesviacionPorcentual = Math.min(minimaDesviacionPorcentual, desviacionActualPorcentual);

        minimaDesviacionPorcentual = calculaMinimaDesviacionPorcentual(valorAnterior, valorPosterior, minimaDesviacionPorcentual);
      }
    }

    return minimaDesviacionPorcentual;
  }

  protected abstract double calculaMinimaDesviacionPorcentual(T valorAnterior, T valorPosterior, double minimaDesviacionPorcentual);

  // private double tantoPorCiento(long parcial, long total) {
  //
  // return (parcial * 100.00) / total;
  // }

}
