package com.gundar.bolsa.importar.datos;

import java.math.BigDecimal;

import com.gundar.bolsa.importar.FuenteDatosDia;

public class ExtractorPrecioApertura implements ExtractorCampo<BigDecimal> {

  public static final ExtractorPrecioApertura INSTANCIA = new ExtractorPrecioApertura();

  @Override
  public BigDecimal extraer(FuenteDatosDia fuenteDatosDia) {

    return fuenteDatosDia.getPrecioApertura();
  }

}
