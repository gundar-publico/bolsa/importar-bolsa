package com.gundar.bolsa.importar.datos;


public class DesviacionPorcentualMaxLong extends DesviacionPorcentualMax<Long> {

  public DesviacionPorcentualMaxLong(double desviacionMaximaPermitida, ExtractorCampo<Long> extractorCampo) {
    super(desviacionMaximaPermitida, extractorCampo);
  }

  @Override
  protected double calculaMaximaDesviacionPorcentual(Long valorAnterior, Long valorPosterior, double maximaDesviacionPorcentual) {

    long diferencia = Math.abs(valorAnterior - valorPosterior);

    double desviacionActualPorcentual = tantoPorCiento(diferencia, valorAnterior);

    maximaDesviacionPorcentual = Math.max(maximaDesviacionPorcentual, desviacionActualPorcentual);

    return maximaDesviacionPorcentual;
  }

  private double tantoPorCiento(long parcial, long total) {

    return (parcial * 100.00) / total;
  }

}
