package com.gundar.bolsa.importar.datos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class DesviacionPorcentualMax<T> extends CalculoDesviacionAbstract<T> {

  private final double desviacionMaximaPermitida;

  public DesviacionPorcentualMax(double desviacionMaximaPermitida, ExtractorCampo<T> extractorCampo) {
    super(extractorCampo);
    this.desviacionMaximaPermitida = desviacionMaximaPermitida;
  }

  @Override
  protected boolean superaDesviacionMaxima(Collection<T> valores) {

    double maximaDesviacion = buscaMayorDesviacion(valores);

    return Double.compare(maximaDesviacion, desviacionMaximaPermitida) > 0;
  }

  private double buscaMayorDesviacion(Collection<T> valores) {

    List<T> valoresLista = new ArrayList<>(valores);

    double maximaDesviacionPorcentual = 0;

    for (int i = 0; i < valoresLista.size(); i++) {

      T valorAnterior = valoresLista.get(i);

      for (int j = i + 1; j < valoresLista.size(); j++) {

        T valorPosterior = valoresLista.get(j);

        // long diferencia = Math.abs(valorAnterior - valorPosterior);
        //
        // double desviacionActualPorcentual = tantoPorCiento(diferencia, valorAnterior);
        //
        // maximaDesviacionPorcentual = Math.max(maximaDesviacionPorcentual, desviacionActualPorcentual);

        maximaDesviacionPorcentual = calculaMaximaDesviacionPorcentual(valorAnterior, valorPosterior, maximaDesviacionPorcentual);
      }
    }

    return maximaDesviacionPorcentual;
  }

  protected abstract double calculaMaximaDesviacionPorcentual(T valorAnterior, T valorPosterior, double maximaDesviacionPorcentual);

  // private double tantoPorCiento(long parcial, long total) {
  //
  // return (parcial * 100.00) / total;
  // }

}
