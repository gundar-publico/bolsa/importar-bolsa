package com.gundar.bolsa.importar.datos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class DesviacionLinealMin<T> extends CalculoDesviacionAbstract<T> {

  // private final long desviacionMaximaPermitida;

  public DesviacionLinealMin(/* long desviacionMaximaPermitida, */ExtractorCampo<T> extractorCampo) {
    super(extractorCampo);
    // this.desviacionMaximaPermitida = desviacionMaximaPermitida;
  }

  @Override
  protected boolean superaDesviacionMaxima(Collection<T> valores) {

    T menorDesviacion = buscaMenorDesviacion(valores);

    // return menorDesviacion > desviacionMaximaPermitida;
    return superaDesviacionMaxima(menorDesviacion);
  }

  private T buscaMenorDesviacion(Collection<T> valores) {

    List<T> valoresLista = new ArrayList<>(valores);

    T minimaDesviacion = null;

    for (int i = 0; i < valoresLista.size(); i++) {

      T valorAnterior = valoresLista.get(i);

      for (int j = i + 1; j < valoresLista.size(); j++) {

        T valorPosterior = valoresLista.get(j);

        // long desviacionActual = Math.abs(valorAnterior - valorPosterior);
        //
        // minimaDesviacion = Math.min(minimaDesviacion, desviacionActual);

        minimaDesviacion = calculaMinimaDesviacion(valorAnterior, valorPosterior, minimaDesviacion);
      }
    }

    return minimaDesviacion;
  }

  protected abstract T calculaMinimaDesviacion(T valorAnterior, T valorPosterior, T minimaDesviacion);

  protected abstract boolean superaDesviacionMaxima(T menorDesviacion);

}
