package com.gundar.bolsa.importar.datos;

import java.math.BigDecimal;

import com.gundar.bolsa.importar.FuenteDatosDia;

public class ExtractorPrecioCierre implements ExtractorCampo<BigDecimal> {

  public static final ExtractorPrecioCierre INSTANCIA = new ExtractorPrecioCierre();

  @Override
  public BigDecimal extraer(FuenteDatosDia fuenteDatosDia) {

    return fuenteDatosDia.getPrecioCierre();
  }

}
