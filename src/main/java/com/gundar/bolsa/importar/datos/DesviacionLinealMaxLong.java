package com.gundar.bolsa.importar.datos;


public class DesviacionLinealMaxLong extends DesviacionLinealMax<Long> {

  private final long desviacionMaximaPermitida;

  public DesviacionLinealMaxLong(long desviacionMaximaPermitida, ExtractorCampo<Long> extractorCampo) {
    super(extractorCampo);
    this.desviacionMaximaPermitida = desviacionMaximaPermitida;
  }

  @Override
  protected Long calculaMaximaDesviacion(Long valorAnterior, Long valorPosterior, Long maximaDesviacion) {

    long desviacionActual = Math.abs(valorAnterior - valorPosterior);

    final Long maximaDesviacionNueva;

    if (maximaDesviacion == null) {
      maximaDesviacionNueva = desviacionActual;
    } else {
      maximaDesviacionNueva = Math.max(maximaDesviacion, desviacionActual);
    }

    return maximaDesviacionNueva;
  }

  @Override
  protected boolean superaDesviacionMaxima(Long maximaDesviacion) {

    return maximaDesviacion > desviacionMaximaPermitida;
  }

}
