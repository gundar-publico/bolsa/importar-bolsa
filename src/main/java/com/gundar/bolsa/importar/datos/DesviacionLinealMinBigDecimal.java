package com.gundar.bolsa.importar.datos;

import java.math.BigDecimal;

public class DesviacionLinealMinBigDecimal extends DesviacionLinealMin<BigDecimal> {

  private final long desviacionMaximaPermitida;

  public DesviacionLinealMinBigDecimal(long desviacionMaximaPermitida, ExtractorCampo<BigDecimal> extractorCampo) {
    super(extractorCampo);
    this.desviacionMaximaPermitida = desviacionMaximaPermitida;
  }

  @Override
  protected BigDecimal calculaMinimaDesviacion(BigDecimal valorAnterior, BigDecimal valorPosterior, BigDecimal minimaDesviacion) {

    // long desviacionActual = Math.abs(valorAnterior - valorPosterior);
    BigDecimal desviacionActual = valorAnterior.subtract(valorPosterior).abs();

    final BigDecimal minimaDesviacionNueva;

    if (minimaDesviacion == null) {
      minimaDesviacionNueva = desviacionActual;
    } else {
      // minimaDesviacionNueva = Math.min(minimaDesviacion, desviacionActual);
      minimaDesviacionNueva = minimaDesviacion.min(desviacionActual);
    }

    return minimaDesviacionNueva;
  }

  @Override
  protected boolean superaDesviacionMaxima(BigDecimal menorDesviacion) {

    return menorDesviacion.compareTo(BigDecimal.valueOf(desviacionMaximaPermitida)) > 0;
    // return menorDesviacion > desviacionMaximaPermitida;
  }

}
