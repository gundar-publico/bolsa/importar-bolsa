package com.gundar.bolsa.importar.datos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class DesviacionLinealMax<T> extends CalculoDesviacionAbstract<T> {

  // private final long desviacionMaximaPermitida;

  public DesviacionLinealMax(/* long desviacionMaximaPermitida, */ExtractorCampo<T> extractorCampo) {
    super(extractorCampo);
    // this.desviacionMaximaPermitida = desviacionMaximaPermitida;
  }

  @Override
  protected boolean superaDesviacionMaxima(Collection<T> valores) {

    T maximaDesviacion = buscaMayorDesviacion(valores);

    // return maximaDesviacion > desviacionMaximaPermitida;
    return superaDesviacionMaxima(maximaDesviacion);
  }

  private T buscaMayorDesviacion(Collection<T> valores) {

    List<T> valoresLista = new ArrayList<>(valores);

    T maximaDesviacion = null;

    for (int i = 0; i < valoresLista.size(); i++) {

      T valorAnterior = valoresLista.get(i);

      for (int j = i + 1; j < valoresLista.size(); j++) {

        T valorPosterior = valoresLista.get(j);

        // long desviacionActual = Math.abs(valorAnterior - valorPosterior);

        // maximaDesviacion = Math.max(maximaDesviacion, desviacionActual);
        maximaDesviacion = calculaMaximaDesviacion(valorAnterior, valorPosterior, maximaDesviacion);
      }
    }

    return maximaDesviacion;
  }

  protected abstract T calculaMaximaDesviacion(T valorAnterior, T valorPosterior, T maximaDesviacion);

  protected abstract boolean superaDesviacionMaxima(T maximaDesviación);

}
