package com.gundar.bolsa.importar.datos;

import com.gundar.bolsa.importar.FuenteDatosDia;

public interface ExtractorCampo<T> {

  public T extraer(FuenteDatosDia fuenteDatosDia);

}
