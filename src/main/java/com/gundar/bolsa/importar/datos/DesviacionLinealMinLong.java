package com.gundar.bolsa.importar.datos;


public class DesviacionLinealMinLong extends DesviacionLinealMin<Long> {

  private final long desviacionMaximaPermitida;

  public DesviacionLinealMinLong(long desviacionMaximaPermitida, ExtractorCampo<Long> extractorCampo) {
    super(extractorCampo);
    this.desviacionMaximaPermitida = desviacionMaximaPermitida;
  }

  @Override
  protected Long calculaMinimaDesviacion(Long valorAnterior, Long valorPosterior, Long minimaDesviacion) {

    long desviacionActual = Math.abs(valorAnterior - valorPosterior);

    final Long minimaDesviacionNueva;

    if (minimaDesviacion == null) {
      minimaDesviacionNueva = desviacionActual;
    } else {
      minimaDesviacionNueva = Math.min(minimaDesviacion, desviacionActual);
    }

    return minimaDesviacionNueva;
  }

  @Override
  protected boolean superaDesviacionMaxima(Long menorDesviacion) {

    return menorDesviacion > desviacionMaximaPermitida;
  }

}
