package com.gundar.bolsa.importar.etf.lyxor;

import java.util.Arrays;
import java.util.Collection;

/**
 * Lyxor IBEX 35 Doble Apalancado Diario UCITS ETF - Acc
 */
public class LyxorFR0011042753 extends LyxorImportador {

  @Override
  protected Collection<String> getNombresArchivos() {
    return Arrays.asList("FR0011042753.csv");
  }

}
