package com.gundar.bolsa.importar.etf.lyxor;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;
import com.gundar.bolsa.importar.ImportadorIndice;
import com.gundar.bolsa.importar.InformacionDia;
import com.gundar.bolsa.importar.etf.FuenteEtf;

public abstract class LyxorImportador extends AbstractImportadorCsv<FuenteEtf> implements ImportadorIndice {

  public static final String RUTA_BASE_ETF = "/home/gun/workspace_oxygen/importar-bolsa/src/main/resources/fuentes/etf/";

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_NAV = 1;

  public LyxorImportador() {
    super(RUTA_BASE_ETF, true);
  }

  @Override
  public List<InformacionDia> importarInformacionDia() throws IOException {

    // List<FuenteEtf> valores = new LyxorImportador(RUTA_BASE_ETF).importar();
    List<FuenteEtf> valores = importar();

    Collections.sort(valores);

    List<InformacionDia> informacionDias = new ArrayList<>(valores.size());

    BigDecimal precioAnterior = null;

    for (FuenteEtf fuenteDia : valores) {

      BigDecimal diferenciaLineal = BigDecimal.ZERO;
      double diferenciaPorcentual = 0;

      if (precioAnterior != null) {

        diferenciaLineal = fuenteDia.getPrecioCierre().subtract(precioAnterior);
        diferenciaPorcentual = diferenciaLineal.doubleValue() / precioAnterior.doubleValue();
      }

      final BigDecimal precioApertura = null;

      InformacionDia infoDia = new InformacionDia(
          fuenteDia.getFecha(),
          fuenteDia.getPrecioCierre(),
          precioApertura,
          diferenciaPorcentual,
          diferenciaLineal);

      precioAnterior = fuenteDia.getPrecioCierre();

      informacionDias.add(infoDia);
    }

    return informacionDias;
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("d/M/yyyy");

  @Override
  protected FuenteEtf convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    LocalDate fecha = LocalDate.parse(dateCsv, FORMATO_FECHA);

    String closeCsv = filtrarString(record.get(CABECERA_NAV));

    final FuenteEtf fuenteDiaIbex35;

    if (closeCsv == null) {

      fuenteDiaIbex35 = new FuenteEtf(fecha);

    } else {

      closeCsv = closeCsv.replace(',', '.');
      BigDecimal valorCierre = new BigDecimal(closeCsv).setScale(2, RoundingMode.HALF_UP);

      fuenteDiaIbex35 = new FuenteEtf(fecha, valorCierre, "Lyxor");
    }

    return fuenteDiaIbex35;
  }

}
