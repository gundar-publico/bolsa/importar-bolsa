package com.gundar.bolsa.importar.etf;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.gundar.bolsa.importar.FuenteDatosDia;

public class FuenteEtf implements FuenteDatosDia {

  private static final BigDecimal VACIO = BigDecimal.valueOf(-1);

  private final LocalDate fecha;
  private final BigDecimal precio;
  private final String origen;

  public FuenteEtf(LocalDate fecha) {
    this(fecha, VACIO, null);
  }

  public FuenteEtf(LocalDate fecha, BigDecimal precio, String origen) {
    super();
    this.fecha = fecha;
    this.precio = precio;
    this.origen = origen;
  }

  @Override
  public int compareTo(FuenteDatosDia o) {

    return fecha.compareTo(o.getFecha());
  }

  @Override
  public boolean esVacio() {
    return precio.compareTo(VACIO) == 0;
  }

  @Override
  public String toString() {
    return "FuenteEtf [fecha=" + fecha + ", precio=" + precio + ", origen=" + origen + "]";
  }

  @Override
  public LocalDate getFecha() {
    return fecha;
  }

  @Override
  public BigDecimal getPrecioCierre() {
    return precio;
  }

  @Override
  public Long getVolumen() {
    return null;
  }

  @Override
  public String getOrigen() {
    return origen;
  }

  @Override
  public BigDecimal getPrecioApertura() {
    return null;
  }

}
