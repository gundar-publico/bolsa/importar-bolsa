package com.gundar.bolsa.importar.etf.lyxor;

import java.util.Arrays;
import java.util.Collection;

/**
 * Lyxor IBEX 35 Inverso Diario UCITS ETF - Acc
 */
public class LyxorFR0010762492 extends LyxorImportador {

  @Override
  protected Collection<String> getNombresArchivos() {
    return Arrays.asList("FR0010762492.csv");
  }

}
