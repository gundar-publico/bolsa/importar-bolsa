package com.gundar.bolsa.importar.etf.lyxor;

import java.util.Arrays;
import java.util.Collection;

/**
 * Lyxor IBEX 35 Doble Inverso Diario UCITS ETF - Acc
 */
public class LyxorFR0011036268 extends LyxorImportador {

  @Override
  protected Collection<String> getNombresArchivos() {
    return Arrays.asList("FR0011036268.csv");
  }

}
