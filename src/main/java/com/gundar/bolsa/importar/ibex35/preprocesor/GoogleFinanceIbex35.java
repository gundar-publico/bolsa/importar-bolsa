package com.gundar.bolsa.importar.ibex35.preprocesor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.ibex35.Ibex35Importador;
import com.gundar.bolsa.importar.parser.ImportadorHttpAbstract;
import com.gundar.bolsa.importar.parser.google.GoogleFinanceParser;

class GoogleFinanceIbex35 extends ImportadorHttpAbstract {

  private static final Logger log = LoggerFactory.getLogger(GoogleFinanceIbex35.class);

  public static final LocalDate FECHA_INICIAL_INDICE = LocalDate.of(2002, 1, 1);
  private static final String CODIGO_IBEX35 = "1054681020857808";

  public GoogleFinanceIbex35() {
    super(new GoogleFinanceParser(CODIGO_IBEX35));
  }

  public static void main(String[] args) throws Exception {

    LocalDate fechaFinal = LocalDate.of(2018, 12, 31);

    new GoogleFinanceIbex35().importarDatosParaArchivo(FECHA_INICIAL_INDICE, fechaFinal);
  }

  @Override
  protected void escribirResultados(List<String> lineasTransformadas) {

    try {

      String rutaArchivo = Ibex35Importador.RUTA_BASE_IBEX35 + "google.csv";

      Files.write(Paths.get(rutaArchivo), lineasTransformadas, StandardOpenOption.CREATE, StandardOpenOption.WRITE);

      if (log.isInfoEnabled()) {
        log.info("Creado archivo = [" + rutaArchivo + "]");
      }

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
