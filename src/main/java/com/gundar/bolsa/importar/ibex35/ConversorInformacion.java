// package com.gundar.bolsa.importar.ibex35;
//
// import java.math.BigDecimal;
// import java.math.MathContext;
// import java.time.LocalDate;
// import java.util.Collection;
//
// import com.gundar.bolsa.InformacionDia;
// import com.gundar.bolsa.datos.SeleccionadorPrecio;
// import com.gundar.bolsa.importar.FuenteDatosDia;
//
// public class ConversorInformacion {
//
// // public static final long PRECIO_VACIO = -1;
//
// private final SeleccionadorPrecio seleccionadorPrecio;
//
// public ConversorInformacion(SeleccionadorPrecio seleccionadorPrecio) {
// super();
// this.seleccionadorPrecio = seleccionadorPrecio;
// }
//
// public InformacionDia convertir(BigDecimal precioAnterior, Collection<? extends FuenteDatosDia> fuentesDia) {
//
// LocalDate fecha = fuentesDia.iterator().next().getFecha();
//
// long precioFuente = seleccionadorPrecio.seleccionar(fuentesDia);
//
// BigDecimal precioActual = BigDecimal.valueOf(precioFuente);
//
// final BigDecimal diferenciaLineal;
// final double diferenciaPorcentual;
//
// if (precioAnterior == null) {
// diferenciaLineal = BigDecimal.ZERO;
// diferenciaPorcentual = 0;
// } else {
// diferenciaLineal = calculaDiferenciaLineal(precioActual, precioAnterior);
// diferenciaPorcentual = calculaDiferenciaPorcentual(diferenciaLineal, precioAnterior);
// }
//
// return new InformacionDia(fecha, precioActual, diferenciaPorcentual, diferenciaLineal);
// }
//
// private BigDecimal calculaDiferenciaLineal(BigDecimal precioActual, BigDecimal precioAnterior) {
//
// return precioActual.subtract(precioAnterior);
// }
//
// private double calculaDiferenciaPorcentual(BigDecimal diferenciaLineal, BigDecimal precioAnterior) {
//
// // long diferencia = precioActual - precioAnterior;
//
// BigDecimal diferenciaPorcentual = diferenciaLineal.divide(precioAnterior, MathContext.DECIMAL128);
//
// return diferenciaPorcentual.doubleValue();
// }
//
// }
