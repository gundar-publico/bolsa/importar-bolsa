package com.gundar.bolsa.importar.ibex35;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;


class LaBolsaImportador extends AbstractImportadorCsv<FuenteDiaIbex35> {

  private static final int CABECERA_FECHA = 0;
  private static final int CABECERA_MAXIMO = 1;
  private static final int CABECERA_MINIMO = 2;
  private static final int CABECERA_CIERRE = 3;
  private static final int CABECERA_VOLUMEN = 4;

  public LaBolsaImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new LaBolsaImportador(Ibex35Importador.RUTA_BASE_IBEX35).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    String rutaBase = "la_bolsa/labolsa_anyo.csv";

    int anyoInicial = 1990;
    int anyoFinal = 2017;

    List<String> rutasArchivos = new ArrayList<>();

    for (int anyoActual = anyoInicial; anyoActual <= anyoFinal; anyoActual++) {

      String rutaArchivo = rutaBase.replace("anyo", String.valueOf(anyoActual));

      rutasArchivos.add(rutaArchivo);
    }

    return rutasArchivos;
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("dd/MM/yyyy");

  @Override
  protected FuenteDiaIbex35 convertirRegistroCsv(CSVRecord record) {

    String fechaCsv = filtrarString(record.get(CABECERA_FECHA));
    LocalDate fecha = null;
    if (fechaCsv != null) {
      fecha = LocalDate.parse(fechaCsv, FORMATO_FECHA);
    }

    String maximoCsv = filtrarString(record.get(CABECERA_MAXIMO));

    String minimoCsv = filtrarString(record.get(CABECERA_MINIMO));

    String cierreCsv = filtrarString(record.get(CABECERA_CIERRE));

    String volumenCsv = filtrarString(record.get(CABECERA_VOLUMEN));

    final FuenteDiaIbex35 fuenteDiaIbex35;

    if (cierreCsv == null) {

      if ((maximoCsv != null) || (minimoCsv != null) || (volumenCsv != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDiaIbex35 = new FuenteDiaIbex35(fecha);

    } else {

      cierreCsv = cierreCsv.replaceAll(",", ".");
      BigDecimal valorCierre = new BigDecimal(cierreCsv).setScale(0, RoundingMode.HALF_UP);

      BigDecimal precioApertura = null;

      Long volumen = null;
      if ((volumenCsv != null) && !"0".equals(volumenCsv)) {
        volumen = Long.valueOf(volumenCsv);
      }

      fuenteDiaIbex35 = new FuenteDiaIbex35(fecha, valorCierre, precioApertura, volumen, "La bolsa");
    }

    return fuenteDiaIbex35;
  }

}
