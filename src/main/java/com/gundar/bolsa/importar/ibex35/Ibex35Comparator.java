package com.gundar.bolsa.importar.ibex35;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.datos.CalculoDesviacion;
import com.gundar.bolsa.importar.datos.DesviacionLinealMinBigDecimal;
import com.gundar.bolsa.importar.datos.DesviacionPorcentualMaxLong;
import com.gundar.bolsa.importar.datos.ExtractorPrecioApertura;
import com.gundar.bolsa.importar.datos.ExtractorPrecioCierre;
import com.gundar.bolsa.importar.datos.ExtractorVolumen;

public class Ibex35Comparator extends Ibex35Importador {

  private static final Logger log = LoggerFactory.getLogger(Ibex35Comparator.class);

  public static void main(String[] args) throws Exception {

    // new Ibex35Comparator().compararPrecios();
    // new Ibex35Comparator().compararVolumen();
    new Ibex35Comparator().compararPreciosApertura();
  }

  public void compararPrecios() throws IOException {

    Map<LocalDate, Collection<FuenteDiaIbex35>> fuentesAgrupadas = cargarAgruparPorDias();

    // CalculoDesviacion desviacion = new DesviacionPorcentual(0.5);
    // CalculoDesviacion desviacion = new DesviacionLinealMax(1);
    CalculoDesviacion desviacion = new DesviacionLinealMinBigDecimal(5, ExtractorPrecioCierre.INSTANCIA);

    int distintos = 0;
    for (Map.Entry<LocalDate, Collection<FuenteDiaIbex35>> diaConFuente : fuentesAgrupadas.entrySet()) {

      boolean desviado = desviacion.valoresDesviados(diaConFuente.getValue());
      if (desviado) {
        distintos++;
      }

    }

    if (log.isWarnEnabled()) {
      log.warn("Total errores = [" + distintos + "]");
    }
  }

  public void compararPreciosApertura() throws IOException {

    Map<LocalDate, Collection<FuenteDiaIbex35>> fuentesAgrupadas = cargarAgruparPorDias();

    // CalculoDesviacion desviacion = new DesviacionPorcentual(0.5);
    // CalculoDesviacion desviacion = new DesviacionLinealMax(1);
    CalculoDesviacion desviacion = new DesviacionLinealMinBigDecimal(1, ExtractorPrecioApertura.INSTANCIA);

    int distintos = 0;
    for (Map.Entry<LocalDate, Collection<FuenteDiaIbex35>> diaConFuente : fuentesAgrupadas.entrySet()) {

      boolean desviado = desviacion.valoresDesviados(diaConFuente.getValue());
      if (desviado) {
        distintos++;
      }

    }

    if (log.isWarnEnabled()) {
      log.warn("Total errores = [" + distintos + "]");
    }
  }

  public void compararVolumen() throws IOException {

    Map<LocalDate, Collection<FuenteDiaIbex35>> fuentesAgrupadas = cargarAgruparPorDias();

    // CalculoDesviacion desviacion = new DesviacionPorcentualMin(1, ExtractorVolumen.INSTANCIA);
    CalculoDesviacion desviacion = new DesviacionPorcentualMaxLong(1, ExtractorVolumen.INSTANCIA);
    // CalculoDesviacion desviacion = new DesviacionLinealMax(1);
    // CalculoDesviacion desviacion = new DesviacionLinealMin(5);

    int distintos = 0;
    for (Map.Entry<LocalDate, Collection<FuenteDiaIbex35>> diaConFuente : fuentesAgrupadas.entrySet()) {

      boolean desviado = desviacion.valoresDesviados(diaConFuente.getValue());
      if (desviado) {
        distintos++;
      }

    }

    if (log.isWarnEnabled()) {
      log.warn("Total errores = [" + distintos + "]");
    }
  }

}
