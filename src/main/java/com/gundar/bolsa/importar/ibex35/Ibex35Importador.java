package com.gundar.bolsa.importar.ibex35;

import static com.gundar.bolsa.importar.ImportadorComun.calculaDiferenciaLineal;
import static com.gundar.bolsa.importar.ImportadorComun.calculaDiferenciaPorcentual;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.ImportadorIndice;
import com.gundar.bolsa.importar.InformacionDia;
import com.gundar.bolsa.importar.datos.ExtractorPrecioApertura;
import com.gundar.bolsa.importar.datos.ExtractorPrecioCierre;
import com.gundar.bolsa.importar.datos.SeleccionadorPrecio;

public class Ibex35Importador implements ImportadorIndice {

  private static final Logger log = LoggerFactory.getLogger(Ibex35Importador.class);

  public static final String RUTA_BASE_IBEX35 = "/home/gun/workspace_oxygen/importar-bolsa/src/main/resources/fuentes/ibex35/";
  protected static final LocalDate FECHA_MAXIMA = LocalDate.of(2017, 12, 31);
  // protected static final LocalDate FECHA_MAXIMA = LocalDate.MAX;

  public static void main(String[] args) throws Exception {

    new Ibex35Importador().importarInformacionDia();
  }

  @Override
  public List<InformacionDia> importarInformacionDia() throws IOException {

    Map<LocalDate, Collection<FuenteDiaIbex35>> fuentesAgrupadas = cargarAgruparPorDias();

    // ConversorInformacion extractorPrecioCierre = new ConversorInformacion(new SeleccionadorPrecio(0.75,
    // ExtractorPrecioCierre.INSTANCIA));

    SeleccionadorPrecio seleccionadorPrecioCierre = new SeleccionadorPrecio(0.75, ExtractorPrecioCierre.INSTANCIA);

    SeleccionadorPrecio seleccionadorPrecioApertura = new SeleccionadorPrecio(0.75, ExtractorPrecioApertura.INSTANCIA);

    List<InformacionDia> informacionTotal = new ArrayList<>();

    // long precioAnterior = ConversorInformacion.PRECIO_VACIO;
    BigDecimal precioCierreAnterior = null;

    for (Map.Entry<LocalDate, Collection<FuenteDiaIbex35>> diaConFuentes : fuentesAgrupadas.entrySet()) {

      LocalDate fecha = diaConFuentes.getKey();

      if (fecha.compareTo(FECHA_MAXIMA) > 0) {

        if (log.isDebugEnabled()) {

          log.debug("La fecha = [" + fecha + "] supera la fecha máxima = [" + FECHA_MAXIMA + "]");
        }

        continue;
      }

      BigDecimal precioCierre = seleccionadorPrecioCierre.seleccionar(diaConFuentes.getValue());

      BigDecimal precioApertura = seleccionadorPrecioApertura.seleccionar(diaConFuentes.getValue());

      final double diferenciaPorcentual;
      final BigDecimal diferenciaLineal;

      if (precioCierreAnterior == null) {
        diferenciaLineal = BigDecimal.ZERO;
        diferenciaPorcentual = 0;
      } else {
        diferenciaLineal = calculaDiferenciaLineal(precioCierre, precioCierreAnterior);
        diferenciaPorcentual = calculaDiferenciaPorcentual(diferenciaLineal.doubleValue(), precioCierreAnterior.doubleValue());
      }

      // InformacionDia informacionDia = extractorPrecioCierre.convertir(precioCierreAnterior, diaConFuentes.getValue());
      InformacionDia informacionDia = new InformacionDia(fecha, precioCierre, precioApertura, diferenciaPorcentual, diferenciaLineal);
      informacionTotal.add(informacionDia);

      precioCierreAnterior = informacionDia.getPrecioCierre();
    }

    return informacionTotal;
  }

  // private BigDecimal calculaDiferenciaLineal(BigDecimal precioActual, BigDecimal precioAnterior) {
  //
  // return precioActual.subtract(precioAnterior);
  // }
  //
  // private double calculaDiferenciaPorcentual(double diferenciaLineal, double precioAnterior) {
  //
  // return diferenciaLineal / precioAnterior;
  // }

  protected Map<LocalDate, Collection<FuenteDiaIbex35>> cargarAgruparPorDias() throws IOException {

    List<FuenteDiaIbex35> valoresInvertia = new InvertiaImportador(RUTA_BASE_IBEX35).importar();

    List<FuenteDiaIbex35> valoresInvesting = new InvestingImportador(RUTA_BASE_IBEX35).importar();

    List<FuenteDiaIbex35> valoresWallStreet = new WallStreetImportador(RUTA_BASE_IBEX35).importar();

    List<FuenteDiaIbex35> valoresYahoo = new YahooImportador(RUTA_BASE_IBEX35).importar();

    List<FuenteDiaIbex35> valoresElMundo = new ElMundoImportador(RUTA_BASE_IBEX35).importar();

    List<FuenteDiaIbex35> valoresInfomercados = new InfomercadosImportador(RUTA_BASE_IBEX35).importar();

    List<FuenteDiaIbex35> valoresLaBolsa = new LaBolsaImportador(RUTA_BASE_IBEX35).importar();

    List<FuenteDiaIbex35> valoresGoogle = new GoogleImportador(RUTA_BASE_IBEX35).importar();

    List<FuenteDiaIbex35> valoresBolsaMadrid = new BolsaMadridImportador(RUTA_BASE_IBEX35).importar();

    Map<LocalDate, Collection<FuenteDiaIbex35>> fuentesAgrupadas = agrupar(
        valoresInvertia,
        valoresInvesting,
        valoresWallStreet,
        valoresYahoo,
        valoresElMundo,
        valoresInfomercados,
        valoresLaBolsa,
        valoresGoogle,
        valoresBolsaMadrid);

    return fuentesAgrupadas;
  }

  @SafeVarargs
  private final Map<LocalDate, Collection<FuenteDiaIbex35>> agrupar(Collection<FuenteDiaIbex35>... todosLosValores) {

    Map<LocalDate, Collection<FuenteDiaIbex35>> informacionPorDia = new TreeMap<>();

    for (Collection<FuenteDiaIbex35> valores : todosLosValores) {

      for (FuenteDiaIbex35 valorDia : valores) {

        Collection<FuenteDiaIbex35> valoresAgrupados = informacionPorDia.get(valorDia.getFecha());

        if (valoresAgrupados == null) {
          valoresAgrupados = new ArrayList<>(todosLosValores.length);
          informacionPorDia.put(valorDia.getFecha(), valoresAgrupados);
        }

        valoresAgrupados.add(valorDia);
      }
    }

    return informacionPorDia;
  }

}
