package com.gundar.bolsa.importar.ibex35;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;


class InvestingImportador extends AbstractImportadorCsv<FuenteDiaIbex35> {

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_PRICE = 1;
  private static final int CABECERA_OPEN = 2;
  private static final int CABECERA_HIGH = 3;
  private static final int CABECERA_LOW = 4;
  private static final int CABECERA_VOLUME = 5;
  private static final int CABECERA_CHANGE = 6;

  public InvestingImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new InvestingImportador(Ibex35Importador.RUTA_BASE_IBEX35).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Arrays.asList("investing_com_1991_2009.csv", "investing_com_2010_2017.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = new DateTimeFormatterBuilder()
      .appendPattern("MMM dd, yyyy")
      .parseCaseInsensitive()
      .toFormatter(Locale.US);

  @Override
  protected FuenteDiaIbex35 convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    LocalDate fecha = null;
    if (dateCsv != null) {
      fecha = LocalDate.parse(dateCsv, FORMATO_FECHA);
    }

    String closeCsv = filtrarString(record.get(CABECERA_PRICE));

    String openCsv = filtrarString(record.get(CABECERA_OPEN));

    String highCsv = filtrarString(record.get(CABECERA_HIGH));

    String lowCsv = filtrarString(record.get(CABECERA_LOW));

    String volumeCsv = filtrarString(record.get(CABECERA_VOLUME));

    String changeCsv = filtrarString(record.get(CABECERA_CHANGE));


    final FuenteDiaIbex35 fuenteDiaIbex35;

    if (closeCsv == null) {

      if ((openCsv != null) || (highCsv != null) || (lowCsv != null) || (volumeCsv != null) || (changeCsv != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDiaIbex35 = new FuenteDiaIbex35(fecha);

    } else {

      closeCsv = closeCsv.replaceAll(",", "");
      BigDecimal valorCierre = new BigDecimal(closeCsv).setScale(0, RoundingMode.HALF_UP);

      openCsv = openCsv.replaceAll(",", "");
      BigDecimal precioApertura = new BigDecimal(openCsv).setScale(0, RoundingMode.HALF_UP);

      Long volume = parseVolumen(volumeCsv);

      fuenteDiaIbex35 = new FuenteDiaIbex35(fecha, valorCierre, precioApertura, volume, "Investing");
    }

    return fuenteDiaIbex35;
  }

  private static char MILLONES = 'M';
  private static char MILLARDOS = 'B';

  private Long parseVolumen(String volumenString) {

    if ((volumenString == null) || "0".equals(volumenString)) {
      return null;
    }

    final String cadenaParaParsear;
    final int multiplicador;

    char ultimoCaracter = volumenString.charAt(volumenString.length() - 1);

    if (ultimoCaracter == MILLONES) {

      cadenaParaParsear = volumenString.substring(0, volumenString.length() - 2);
      multiplicador = 1_000_000;

    } else if (ultimoCaracter == MILLARDOS) {

      cadenaParaParsear = volumenString.substring(0, volumenString.length() - 2);
      multiplicador = 1_000_000_000;

    } else {

      cadenaParaParsear = volumenString;
      multiplicador = 1;
    }

    double volumen = Double.parseDouble(cadenaParaParsear);

    volumen = volumen * multiplicador;

    return Math.round(volumen);
  }

}
