package com.gundar.bolsa.importar.ibex35;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;


class InfomercadosImportador extends AbstractImportadorCsv<FuenteDiaIbex35> {

  private static final int CABECERA_FECHA = 1;
  private static final int CABECERA_APERTURA = 2;
  private static final int CABECERA_MAX = 3;
  private static final int CABECERA_MIN = 4;
  private static final int CABECERA_ULTIMO = 5;
  private static final int CABECERA_VOLUMEN = 6;

  public InfomercadosImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new InfomercadosImportador(Ibex35Importador.RUTA_BASE_IBEX35).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Arrays.asList("infomercados_ibex.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("dd/MM/yyyy");

  @Override
  protected FuenteDiaIbex35 convertirRegistroCsv(CSVRecord record) {

    String fechaCsv = filtrarString(record.get(CABECERA_FECHA));
    LocalDate fecha = null;
    if (fechaCsv != null) {
      fecha = LocalDate.parse(fechaCsv, FORMATO_FECHA);
    }

    String cierreCsv = filtrarString(record.get(CABECERA_ULTIMO));

    String aperturaCsv = filtrarString(record.get(CABECERA_APERTURA));

    String maximoCsv = filtrarString(record.get(CABECERA_MAX));

    String minimoCsv = filtrarString(record.get(CABECERA_MIN));

    String volumenCsv = filtrarString(record.get(CABECERA_VOLUMEN));


    final FuenteDiaIbex35 fuenteDiaIbex35;

    if (cierreCsv == null) {

      if ((aperturaCsv != null) || (maximoCsv != null) || (minimoCsv != null) || (volumenCsv != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDiaIbex35 = new FuenteDiaIbex35(fecha);

    } else {

      // double valorCierre = Double.parseDouble(cierreCsv);
      BigDecimal valorCierre = new BigDecimal(cierreCsv).setScale(0, RoundingMode.HALF_UP);

      BigDecimal precioApertura = new BigDecimal(aperturaCsv).setScale(0, RoundingMode.HALF_UP);

      Long volumen = null;
      if ((volumenCsv != null) && !"0".equals(volumenCsv)) {
        volumen = Long.valueOf(volumenCsv);
      }

      fuenteDiaIbex35 = new FuenteDiaIbex35(fecha, valorCierre, precioApertura, volumen, "Infomercados");
    }

    return fuenteDiaIbex35;
  }

}
