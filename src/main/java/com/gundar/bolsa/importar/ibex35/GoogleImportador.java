package com.gundar.bolsa.importar.ibex35;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;

class GoogleImportador extends AbstractImportadorCsv<FuenteDiaIbex35> {

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_OPEN = 1;
  private static final int CABECERA_HIGH = 2;
  private static final int CABECERA_LOW = 3;
  private static final int CABECERA_CLOSE = 4;
  private static final int CABECERA_VOLUME = 5;

  public GoogleImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, false);
  }

  public static void main(String[] args) throws Exception {

    new GoogleImportador(Ibex35Importador.RUTA_BASE_IBEX35).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Collections.singletonList("google.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ISO_LOCAL_DATE;

  @Override
  protected FuenteDiaIbex35 convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    LocalDate fecha = LocalDate.parse(dateCsv, FORMATO_FECHA);

    String openCsv = filtrarString(record.get(CABECERA_OPEN));

    String highCsv = filtrarString(record.get(CABECERA_HIGH));

    String lowCsv = filtrarString(record.get(CABECERA_LOW));

    String closeCsv = filtrarString(record.get(CABECERA_CLOSE));

    String volumeCsv = filtrarString(record.get(CABECERA_VOLUME));

    final FuenteDiaIbex35 fuenteDatosDia;

    if (closeCsv == null) {

      if ((openCsv != null) || (highCsv != null) || (lowCsv != null) || (volumeCsv != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDatosDia = new FuenteDiaIbex35(fecha);

    } else {

      // double valorCierre = Double.parseDouble(closeCsv);
      BigDecimal valorCierre = new BigDecimal(closeCsv).setScale(0, RoundingMode.HALF_UP);

      BigDecimal precioApertura = new BigDecimal(openCsv).setScale(0, RoundingMode.HALF_UP);

      Long volume = null;
      if ((volumeCsv != null) && !"0".equals(volumeCsv)) {
        volume = Long.valueOf(volumeCsv);
      }

      fuenteDatosDia = new FuenteDiaIbex35(fecha, valorCierre, precioApertura, volume, "Google");
    }

    return fuenteDatosDia;
  }

}
