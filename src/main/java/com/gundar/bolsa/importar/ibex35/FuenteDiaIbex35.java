package com.gundar.bolsa.importar.ibex35;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.gundar.bolsa.importar.FuenteDatosDia;

class FuenteDiaIbex35 implements FuenteDatosDia {

  private static final long VACIO = -1;
  private static final BigDecimal VACIO_BIG = BigDecimal.valueOf(VACIO);

  private LocalDate fecha;
  private BigDecimal precioCierre;
  private BigDecimal precioApertura;
  private Long volumen;
  private String origen;

  public FuenteDiaIbex35(LocalDate fecha) {
    this(fecha, VACIO_BIG, null, null, null);
  }

  // public FuenteDiaIbex35(LocalDate fecha, double precioCierre, Double volumen, String origen) {
  // super();
  // this.fecha = fecha;
  // this.valorCierre = redondear(precioCierre);
  // this.volumen = redondear(volumen);
  // this.origen = origen;
  // }

  public FuenteDiaIbex35(LocalDate fecha, BigDecimal precioCierre, BigDecimal precioApertura, Long volumen, String origen) {
    super();
    this.fecha = fecha;
    this.precioCierre = precioCierre;
    this.precioApertura = precioApertura;
    this.volumen = volumen;
    this.origen = origen;
  }

  private Long redondear(Double valor) {

    Long valorRedondeado = null;
    if (valor != null) {
      valorRedondeado = Math.round(valor);
    }

    return valorRedondeado;
  }

  @Override
  public boolean esVacio() {
    // return Long.compare(precioCierre, VACIO) == 0;
    return precioCierre.compareTo(VACIO_BIG) == 0;
  }

  @Override
  public int compareTo(FuenteDatosDia otraInfo) {
    return fecha.compareTo(otraInfo.getFecha());
  }

  @Override
  public BigDecimal getPrecioCierre() {
    return precioCierre;
  }

  @Override
  public String toString() {
    return "FuenteDiaIbex35 [fecha=" + fecha + ", precioCierre=" + precioCierre + ", precioApertura=" + precioApertura + ", volumen="
        + volumen + ", origen=" + origen + "]";
  }

  @Override
  public LocalDate getFecha() {
    return fecha;
  }

  @Override
  public Long getVolumen() {
    return volumen;
  }

  @Override
  public String getOrigen() {
    return origen;
  }

  @Override
  public BigDecimal getPrecioApertura() {
    return precioApertura;
  }

}
