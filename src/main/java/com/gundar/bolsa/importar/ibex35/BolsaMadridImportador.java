package com.gundar.bolsa.importar.ibex35;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;


class BolsaMadridImportador extends AbstractImportadorCsv<FuenteDiaIbex35> {

  private static final int CABECERA_FECHA = 0;
  private static final int CABECERA_ULTIMO = 1;
  private static final int CABECERA_ANTERIOR = 2;
  private static final int CABECERA_MAXIMO = 3;
  private static final int CABECERA_MINIMO = 4;
  private static final int CABECERA_MEDIA = 5;

  public BolsaMadridImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new BolsaMadridImportador(Ibex35Importador.RUTA_BASE_IBEX35).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Arrays.asList("bolsamadrid.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("dd/MM/yyyy");

  @Override
  protected FuenteDiaIbex35 convertirRegistroCsv(CSVRecord record) {

    String fechaCsv = filtrarString(record.get(CABECERA_FECHA));
    LocalDate fecha = null;
    if (fechaCsv != null) {
      fecha = LocalDate.parse(fechaCsv, FORMATO_FECHA);
    }

    String ultimoCsv = filtrarString(record.get(CABECERA_ULTIMO));

    String anteriorCsv = filtrarString(record.get(CABECERA_ANTERIOR));

    String maximoCsv = filtrarString(record.get(CABECERA_MAXIMO));

    String minimoCsv = filtrarString(record.get(CABECERA_MINIMO));

    String mediaCsv = filtrarString(record.get(CABECERA_MEDIA));

    final FuenteDiaIbex35 fuenteDiaIbex35;

    if (ultimoCsv == null) {

      if ((anteriorCsv != null) || (maximoCsv != null) || (minimoCsv != null) || (mediaCsv != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDiaIbex35 = new FuenteDiaIbex35(fecha);

    } else {

      ultimoCsv = ultimoCsv.replaceAll(",", "");
      // double valorCierre = Double.parseDouble(closeCsv);
      BigDecimal valorCierre = new BigDecimal(ultimoCsv).setScale(2, RoundingMode.HALF_UP);

      BigDecimal precioApertura = null;

      final Long volume = null;

      fuenteDiaIbex35 = new FuenteDiaIbex35(fecha, valorCierre, precioApertura, volume, "BolsaMadrid");
    }

    return fuenteDiaIbex35;
  }

}
