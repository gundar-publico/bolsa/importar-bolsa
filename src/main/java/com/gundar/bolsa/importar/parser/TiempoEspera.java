package com.gundar.bolsa.importar.parser;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TiempoEspera {

  public static final TiempoEspera NO_ESPERA = new TiempoEspera(0, 0);
  private static final Logger log = LoggerFactory.getLogger(TiempoEspera.class);

  private final Random random = new Random();
  private final int tiempoMinimo;
  private final int deltaMaximo;

  private long ultimaPeticion = 0;

  public TiempoEspera(int tiempoMinimoMs, int tiempoMaximoMs) {
    super();
    this.tiempoMinimo = tiempoMinimoMs;
    this.deltaMaximo = tiempoMaximoMs - tiempoMinimoMs;
  }

  public void esperar() throws InterruptedException {

    if ((tiempoMinimo == 0) && (deltaMaximo == 0)) {
      return;
    }

    int deltaActual = random.nextInt(deltaMaximo);
    long tiempoEspera = tiempoMinimo + deltaActual;

    long tiempoDesdeUltimaPeticion = System.currentTimeMillis() - ultimaPeticion;

    if (tiempoEspera > tiempoDesdeUltimaPeticion) {

      tiempoEspera = tiempoEspera - tiempoDesdeUltimaPeticion;
      TimeUnit.MILLISECONDS.sleep(tiempoEspera);
    } else {
      tiempoEspera = 0;
    }

    if (log.isDebugEnabled()) {
      log.debug("Ultima peticion = [" + tiempoDesdeUltimaPeticion + "], esperamos = [" + tiempoEspera + "], total = ["
          + (tiempoDesdeUltimaPeticion + tiempoEspera) + "]");
    }

    ultimaPeticion = System.currentTimeMillis();
  }

}
