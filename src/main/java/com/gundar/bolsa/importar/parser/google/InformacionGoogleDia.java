package com.gundar.bolsa.importar.parser.google;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.gundar.bolsa.importar.parser.Csvable;

class InformacionGoogleDia implements Csvable {

  private LocalDate fecha;
  private BigDecimal open;
  private BigDecimal high;
  private BigDecimal low;
  private BigDecimal close;
  private Long volume;

  public InformacionGoogleDia(LocalDate fecha, BigDecimal open, BigDecimal high, BigDecimal low, BigDecimal close, Long volume) {
    super();
    this.fecha = fecha;
    this.open = open;
    this.high = high;
    this.low = low;
    this.close = close;
    this.volume = volume;
  }

  @Override
  public String toLineaCsv() {

    return fecha.format(DateTimeFormatter.ISO_LOCAL_DATE) + SEPARADOR_CSV
        + open + SEPARADOR_CSV
        + high + SEPARADOR_CSV
        + low + SEPARADOR_CSV
        + close + SEPARADOR_CSV
        + volume;
  }

  @Override
  public String toString() {
    return "InformacionAdvfnDia [fecha=" + fecha + ", open=" + open + ", high=" + high + ", low=" + low + ", close=" + close + ", volume="
        + volume
        + "]";
  }

  @Override
  public LocalDate getFecha() {
    return fecha;
  }

}
