package com.gundar.bolsa.importar.parser;

import java.time.LocalDate;
import java.util.List;

public interface ParserHttp {

  public List<? extends Csvable> recuperarDatos(final LocalDate fechaInicial, final LocalDate fechaFinal) throws Exception;

}
