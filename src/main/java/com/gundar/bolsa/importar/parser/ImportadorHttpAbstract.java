package com.gundar.bolsa.importar.parser;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class ImportadorHttpAbstract {

  private final ParserHttp parser;

  public ImportadorHttpAbstract(ParserHttp parser) {
    super();
    this.parser = parser;
  }

  public void importarDatosParaArchivo(LocalDate fechaInicial, LocalDate fechaFinal) throws Exception {

    List<? extends Csvable> filasPrecios = parser.recuperarDatos(fechaInicial, fechaFinal);

    Collections.sort(filasPrecios, new OrdenFecha());

    List<String> lineasArchivo = transformarLineas(filasPrecios);

    escribirResultados(lineasArchivo);
  }

  private List<String> transformarLineas(Collection<? extends Csvable> filasPrecios) {

    List<String> lineasArchivo = new ArrayList<>(filasPrecios.size());

    for (Csvable informacionDia : filasPrecios) {

      String lineaArchivo = informacionDia.toLineaCsv();

      lineasArchivo.add(lineaArchivo);
    }

    return lineasArchivo;
  }

  protected abstract void escribirResultados(List<String> lineasTransformadas);

  private static class OrdenFecha implements Comparator<Csvable> {

    @Override
    public int compare(Csvable c1, Csvable c2) {

      return c1.getFecha().compareTo(c2.getFecha());
    }

  }

}
