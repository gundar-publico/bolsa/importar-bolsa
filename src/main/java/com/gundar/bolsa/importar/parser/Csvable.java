package com.gundar.bolsa.importar.parser;

import java.time.LocalDate;

public interface Csvable {

  public static final String SEPARADOR_CSV = ",";

  public LocalDate getFecha();

  public String toLineaCsv();

}
