package com.gundar.bolsa.importar.parser.google;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.parser.Csvable;
import com.gundar.bolsa.importar.parser.ParserHttp;

public class GoogleFinanceParser implements ParserHttp {

  private static final Logger log = LoggerFactory.getLogger(GoogleFinanceParser.class);
  private static final DateTimeFormatter FORMATO_FECHA_PETICION = DateTimeFormatter.ofPattern("MMM d, yyy", Locale.US);

  private static final String url = "https://finance.google.com/finance/historical";
  private static final int LOTE = 30;
  private static final String CAMPO_VACIO = "-";

  private final String codigoIndiceBolsa;

  public GoogleFinanceParser(String codigoIndiceBolsa) {
    super();
    this.codigoIndiceBolsa = codigoIndiceBolsa;
  }

  public static void main(String[] args) throws Exception {

    final String cid = "626307";// cid SP500

    LocalDate fechaInicial = LocalDate.of(2017, 3, 1);

    LocalDate fechaFinal = LocalDate.of(2018, 2, 1);

    new GoogleFinanceParser(cid).recuperarDatos(fechaInicial, fechaFinal);
  }

  @Override
  public List<? extends Csvable> recuperarDatos(final LocalDate fechaInicial, final LocalDate fechaFinal) throws IOException {

    Set<LocalDate> fechasConsultadas = new HashSet<>();

    List<InformacionGoogleDia> preciosTotales = new ArrayList<>();
    List<InformacionGoogleDia> preciosLote = Collections.emptyList();

    long tiempoInicial = System.nanoTime();
    int numeroPeticiones = 0;

    int indicePaginacion = 0;

    while (true) {

      preciosLote = recuperarPreciosLote(fechaInicial, fechaFinal, indicePaginacion);
      numeroPeticiones++;

      /* Si ya no vienen resutados hemos llegado al final */
      if (preciosLote.isEmpty()) {
        break;
      }

      LocalDate fechaInicialLote = null;
      LocalDate fechaFinalLote = null;

      for (InformacionGoogleDia nuevoPrecio : preciosLote) {

        boolean fechaYaConsultada = !fechasConsultadas.add(nuevoPrecio.getFecha());

        if (fechaYaConsultada) {
          throw new IllegalStateException("Fecha ya consultada = [" + nuevoPrecio + "]");
        }

        preciosTotales.add(nuevoPrecio);

        if (fechaInicialLote == null) {
          fechaInicialLote = nuevoPrecio.getFecha();
        }
        fechaFinalLote = nuevoPrecio.getFecha();

      }

      if (log.isInfoEnabled()) {
        log.info(
            "Peticion para el índice = [" + indicePaginacion + "] con resultados entre = [" + fechaInicialLote + " - " + fechaFinalLote
                + "]");
      }

      indicePaginacion += LOTE;
    }

    if (log.isInfoEnabled()) {
      long duracionMs = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - tiempoInicial);
      log.info("Se han lanzado [" + numeroPeticiones + "] peticiones tardando = [" + duracionMs + "] seg entre las fechas = ["
          + fechaInicial + " - " + fechaFinal + "]");
    }

    return preciosTotales;
  }

  private List<InformacionGoogleDia> recuperarPreciosLote(final LocalDate fechaInicial, final LocalDate fechaFinal, int indice)
      throws IOException {

    Document pagina = cargarPagina(fechaInicial, fechaFinal, indice);

    Element tablaPreciosHtml = buscarTablaPrecios(pagina);

    final List<InformacionGoogleDia> filasPrecios;

    /* Puede no haber tabla de precios para las fechas indicadas */
    if (tablaPreciosHtml == null) {
      filasPrecios = Collections.emptyList();
    } else {
      filasPrecios = convertirTablaHtmlToJava(tablaPreciosHtml);
    }

    return filasPrecios;
  }

  private Document cargarPagina(LocalDate fechaInicial, LocalDate fechaFinal, int indice) throws IOException {

    Connection conexion = Jsoup.connect(url);

    conexion.data("startdate", fechaInicial.format(FORMATO_FECHA_PETICION));
    conexion.data("enddate", fechaFinal.format(FORMATO_FECHA_PETICION));
    conexion.data("cid", codigoIndiceBolsa);
    conexion.data("start", String.valueOf(indice));
    conexion.data("num", String.valueOf(LOTE));

    conexion.userAgent(HttpConnection.DEFAULT_UA);

    return conexion.get();
  }

  private Element buscarTablaPrecios(Document pagina) {

    Element divPrices = pagina.getElementById("prices");

    if (divPrices == null) {
      return null;
    }

    int numeroElementosHijos = divPrices.children().size();

    if (numeroElementosHijos != 1) {
      throw new IllegalStateException("Más de una tabla de precios. Tablas = [" + numeroElementosHijos + "]");
    }

    Element tablaPreciosHtml = divPrices.children().iterator().next();

    return tablaPreciosHtml;
  }

  private List<InformacionGoogleDia> convertirTablaHtmlToJava(Element tablaPreciosHtml) {

    final String FILA_TABLA_HTML = "tr";

    Elements filasPreciosHtml = tablaPreciosHtml.select(FILA_TABLA_HTML);

    List<InformacionGoogleDia> filasPrecios = new ArrayList<>(filasPreciosHtml.size());

    for (Element filaHtml : filasPreciosHtml) {

      if (!esCabecera(filaHtml)) {

        InformacionGoogleDia informacionGoogleDia = crearFilaPrecios(filaHtml);
        filasPrecios.add(informacionGoogleDia);
      }
    }

    return filasPrecios;
  }

  private boolean esCabecera(Element filaTablaHtml) {

    final String CABECERA_TABLA_HTML = "th";

    return filaTablaHtml.selectFirst(CABECERA_TABLA_HTML) != null;
  }

  private static final int COLUMNA_DATE = 0;
  private static final int COLUMNA_OPEN = 1;
  private static final int COLUMNA_HIGH = 2;
  private static final int COLUMNA_LOW = 3;
  private static final int COLUMNA_CLOSE = 4;
  private static final int COLUMNA_VOLUME = 5;

  private InformacionGoogleDia crearFilaPrecios(Element filaPreciosHtml) {

    final String DATOS_TABLA_HTML = "td";

    Elements camposFilaHtml = filaPreciosHtml.select(DATOS_TABLA_HTML);

    if (camposFilaHtml.size() != 6) {
      throw new IllegalStateException(
          "Se esperaban 6 campos, recibidos = [" + camposFilaHtml.size() + "]. Fila = [" + filaPreciosHtml + "]");
    }

    Element dateHtml = camposFilaHtml.get(COLUMNA_DATE);
    LocalDate fecha = LocalDate.parse(dateHtml.text(), FORMATO_FECHA_PETICION);

    Element openHtml = camposFilaHtml.get(COLUMNA_OPEN);
    BigDecimal open = parseDecimal(openHtml);

    Element highHtml = camposFilaHtml.get(COLUMNA_HIGH);
    BigDecimal high = parseDecimal(highHtml);

    Element lowHtml = camposFilaHtml.get(COLUMNA_LOW);
    BigDecimal low = parseDecimal(lowHtml);

    Element closeHtml = camposFilaHtml.get(COLUMNA_CLOSE);
    BigDecimal close = parseDecimal(closeHtml);

    Element volumeHtml = camposFilaHtml.get(COLUMNA_VOLUME);
    Long volume = null;

    if (!esCampoVacio(volumeHtml.text())) {
      String volumeFiltrado = filtrarSeparadorMiles(volumeHtml.text());
      volume = Long.parseLong(volumeFiltrado);
    }

    return new InformacionGoogleDia(fecha, open, high, low, close, volume);
  }

  private BigDecimal parseDecimal(Element elementoDecimalHtml) {

    BigDecimal valorDecimal = null;

    if (!esCampoVacio(elementoDecimalHtml.text())) {

      String decimalFiltrado = filtrarSeparadorMiles(elementoDecimalHtml.text());

      valorDecimal = new BigDecimal(decimalFiltrado);
    }

    return valorDecimal;
  }

  private boolean esCampoVacio(String valorCampo) {
    return CAMPO_VACIO.equals(valorCampo);
  }

  private String filtrarSeparadorMiles(String valorOriginal) {

    return valorOriginal.replace(",", "");
  }

}
