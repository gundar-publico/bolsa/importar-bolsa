package com.gundar.bolsa.importar.parser.advfn;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.parser.Csvable;
import com.gundar.bolsa.importar.parser.ParserHttp;
import com.gundar.bolsa.importar.parser.TiempoEspera;

public class AdvfnParser implements ParserHttp {

  private static final Logger log = LoggerFactory.getLogger(AdvfnParser.class);
  private static final DateTimeFormatter FORMATO_FECHA_PETICION = DateTimeFormatter.ofPattern("dd/MM/yy");
  private static final DateTimeFormatter FORMATO_FECHA_RESPUESTA = new DateTimeFormatterBuilder()
      .parseCaseInsensitive()
      .appendPattern("dd MMM yyyy")
      .toFormatter(Locale.forLanguageTag("es-ES"));

  private final String url;
  private static final String CAMPO_VACIO = "-";

  private final TiempoEspera espera = new TiempoEspera(10000, 15000);

  // private final Random random = new Random(System.currentTimeMillis());

  // private long ultimaPeticion;

  public AdvfnParser(String urlIndice) {
    super();
    this.url = urlIndice;
  }

  public static void main(String[] args) throws Exception {

    // System.out.println(LocalDate.of(2018, 02, 01).format(FORMATO_FECHA_RESPUESTA));

    // System.out.println(Arrays.toString(Locale.getAvailableLocales()));

    // LocalDate a = LocalDate.parse("01 Feb 2018", FORMATO_FECHA_RESPUESTA);
    // System.out.println(a);

    // LocalDate fechaInicial = LocalDate.of(1970, 1, 1);
    //
    // LocalDate fechaFinal = LocalDate.of(2017, 12, 31);
    //
    // new AdvfnParser(cid).recuperarDatos(fechaInicial, fechaFinal);
  }

  @Override
  public List<? extends Csvable> recuperarDatos(final LocalDate fechaInicial, final LocalDate fechaFinal)
      throws IOException, InterruptedException {

    LocalDate fechaInicialLote = null;
    LocalDate fechaFinalLote = null;

    List<InformacionAdvfnDia> resultadosTotales = new ArrayList<>();

    boolean finalizar = false;

    while (!finalizar) {

      fechaInicialLote = fechaInicialLote(fechaInicial, fechaFinalLote);
      fechaFinalLote = fechaFinalLote(fechaFinal, fechaInicialLote);

      if (fechaFinalLote.compareTo(fechaFinal) == 0) {
        finalizar = true;
      }

      List<InformacionAdvfnDia> resultadosLote = recuperarDatosPorLotes(fechaInicialLote, fechaFinalLote);
      resultadosTotales.addAll(resultadosLote);
    }

    return resultadosTotales;

  }

  private LocalDate fechaInicialLote(LocalDate fechaInicial, LocalDate fechaFinalLoteAnterior) {

    final LocalDate fechaInicialLote;

    if (fechaFinalLoteAnterior == null) {
      fechaInicialLote = fechaInicial;
    } else {
      fechaInicialLote = fechaFinalLoteAnterior.plusDays(1);
    }

    return fechaInicialLote;
  }

  private LocalDate fechaFinalLote(LocalDate fechaFinal, LocalDate fechaInicialLoteActual) {

    final int LOTE_ANYOS = 5;

    LocalDate fechaFinalLote = fechaInicialLoteActual.plusYears(LOTE_ANYOS);

    if (fechaFinalLote.isAfter(fechaFinal)) {
      fechaFinalLote = fechaFinal;
    }

    return fechaFinalLote;
  }


  public List<InformacionAdvfnDia> recuperarDatosPorLotes(final LocalDate fechaInicial, final LocalDate fechaFinal)
      throws IOException, InterruptedException {

    Set<LocalDate> fechasConsultadas = new HashSet<>();

    List<InformacionAdvfnDia> preciosTotales = new ArrayList<>();
    List<InformacionAdvfnDia> preciosLote = Collections.emptyList();

    long tiempoInicial = System.nanoTime();
    int numeroPeticiones = 0;

    int current = 0;
    LocalDate ultimaFechaRepetida = LocalDate.MIN;

    peticiones:
    while (true) {

      preciosLote = recuperarPreciosLote(fechaInicial, fechaFinal, current);
      // preciosLote = Arrays.asList(new InformacionAdvfnDia(LocalDate.now(), null, null, null, null, null, null, null));
      numeroPeticiones++;

      /* Si ya no vienen resutados hemos llegado al final */
      if (preciosLote.isEmpty()) {
        break;
      }

      /* Si viene únicamente el precio inicial también hemos llegado al final */
      if ((preciosLote.size() == 1) && (preciosLote.get(0).getFecha().compareTo(fechaFinal) == 0)) {
        break;
      }

      LocalDate fechaInicialLote = null;
      LocalDate fechaFinalLote = null;

      boolean primerElementoRespuesta = true;

      for (InformacionAdvfnDia nuevoPrecio : preciosLote) {

        boolean fechaYaConsultada = !fechasConsultadas.add(nuevoPrecio.getFecha());

        if (fechaYaConsultada) {

          if (primerElementoRespuesta) {
            if (nuevoPrecio.getFecha().compareTo(ultimaFechaRepetida) == 0) {
              break peticiones;
            } else {
              ultimaFechaRepetida = nuevoPrecio.getFecha();
              continue;
            }
          } else {
            throw new IllegalStateException("Fecha ya consultada = [" + nuevoPrecio + "]");
          }

        }

        primerElementoRespuesta = false;

        preciosTotales.add(nuevoPrecio);

        if (fechaInicialLote == null) {
          fechaInicialLote = nuevoPrecio.getFecha();
        }
        fechaFinalLote = nuevoPrecio.getFecha();

      }

      // if((fechaInicialLote == null) && (fechaFinalLote == null)) {
      // break;
      // }

      if (log.isInfoEnabled()) {
        log.info("Peticion para el índice = [" + current + "] con resultados entre = ["
            + fechaInicialLote + " - " + fechaFinalLote + "]");
      }

      current++;
    }

    if (log.isInfoEnabled()) {
      long duracionMs = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - tiempoInicial);
      log.info("Se han lanzado [" + numeroPeticiones + "] peticiones tardando = [" + duracionMs + "] seg entre las fechas = ["
          + fechaInicial + " - " + fechaFinal + "]");
    }

    return preciosTotales;
  }

  private List<InformacionAdvfnDia> recuperarPreciosLote(final LocalDate fechaInicial, final LocalDate fechaFinal, int current)
      throws IOException, InterruptedException {

    Document pagina = cargarPagina(fechaInicial, fechaFinal, current);

    Element tablaPreciosHtml = buscarTablaPrecios(pagina);

    final List<InformacionAdvfnDia> filasPrecios;

    /* Puede no haber tabla de precios para las fechas indicadas */
    if (tablaPreciosHtml == null) {
      filasPrecios = Collections.emptyList();
    } else {
      filasPrecios = convertirTablaHtmlToJava(tablaPreciosHtml);
    }

    return filasPrecios;
  }

  private Document cargarPagina(LocalDate fechaInicial, LocalDate fechaFinal, int current) throws IOException, InterruptedException {

    espera.esperar();

    Connection conexion = Jsoup.connect(url);

    conexion.data("Date1", fechaInicial.format(FORMATO_FECHA_PETICION));
    conexion.data("Date2", fechaFinal.format(FORMATO_FECHA_PETICION));
    conexion.data("current", String.valueOf(current));

    conexion.userAgent(HttpConnection.DEFAULT_UA);

    return conexion.get();
  }

  // private void esperar() throws InterruptedException {
  //
  // final int tiempoMinimo = (int) TimeUnit.SECONDS.toMillis(5);
  // final int deltaMaximo = (int) TimeUnit.SECONDS.toMillis(7);
  //
  // int deltaActual = random.nextInt(deltaMaximo);
  // long nuevaEspera = tiempoMinimo + deltaActual;
  //
  // long tiempoDesdeUltimaPeticion = System.currentTimeMillis() - ultimaPeticion;
  //
  // if (nuevaEspera > tiempoDesdeUltimaPeticion) {
  // nuevaEspera = nuevaEspera - tiempoDesdeUltimaPeticion;
  // TimeUnit.MILLISECONDS.sleep(nuevaEspera);
  // } else {
  // nuevaEspera = 0;
  // }
  //
  // if (log.isInfoEnabled()) {
  // log.info("Ultima peticion = [" + tiempoDesdeUltimaPeticion + "], esperamos = [" + nuevaEspera + "], total = ["
  // + (tiempoDesdeUltimaPeticion + nuevaEspera) + "]");
  // }
  //
  // ultimaPeticion = System.currentTimeMillis();
  // }

  private Element buscarTablaPrecios(Document pagina) {

    Elements tablasPrecios = pagina.getElementsByClass("TableElement");

    if (tablasPrecios == null) {
      return null;
    }

    if (tablasPrecios.size() > 1) {
      throw new IllegalStateException("Más de una tabla de precios. Tablas = [" + tablasPrecios.size() + "]");
    }

    return tablasPrecios.iterator().next();
  }

  private List<InformacionAdvfnDia> convertirTablaHtmlToJava(Element tablaPreciosHtml) {

    final String FILA_TABLA_HTML = "tr";

    Elements filasPreciosHtml = tablaPreciosHtml.select(FILA_TABLA_HTML);

    List<InformacionAdvfnDia> filasPrecios = new ArrayList<>(filasPreciosHtml.size());

    for (Element filaHtml : filasPreciosHtml) {

      if (!esCabecera(filaHtml)) {

        Optional<InformacionAdvfnDia> informacionAdvfnDia = crearFilaPrecios(filaHtml);

        if (informacionAdvfnDia.isPresent()) {
          filasPrecios.add(informacionAdvfnDia.get());
        }
      }
    }

    return filasPrecios;
  }

  private boolean esCabecera(Element filaTablaHtml) {

    final String CABECERA_TABLA_HTML = "th";

    return filaTablaHtml.selectFirst(CABECERA_TABLA_HTML) != null;
  }

  private static final int COLUMNA_FECHA = 0;
  private static final int COLUMNA_APERTURA = 1;
  private static final int COLUMNA_CIERRE = 2;
  private static final int COLUMNA_VARIACION = 3;
  private static final int COLUMNA_VARIACION_PORCENTUAL = 4;
  private static final int COLUMNA_MINIMO = 5;
  private static final int COLUMNA_MAXIMO = 6;
  private static final int COLUMNA_VOLUMEN = 7;

  private Optional<InformacionAdvfnDia> crearFilaPrecios(Element filaPreciosHtml) {

    final String DATOS_TABLA_HTML = "td";

    Elements camposFilaHtml = filaPreciosHtml.select(DATOS_TABLA_HTML);

    if (camposFilaHtml.size() != 8) {

      if ("No Results Found for the Dates Provided".equals(camposFilaHtml.text())) {
        return Optional.empty();
      } else {
        throw new IllegalStateException(
            "Se esperaban 6 campos, recibidos = [" + camposFilaHtml.size() + "]. Fila = [" + filaPreciosHtml + "]");
      }
    }

    Element fechaHtml = camposFilaHtml.get(COLUMNA_FECHA);
    LocalDate fecha = LocalDate.parse(fechaHtml.text(), FORMATO_FECHA_RESPUESTA);

    Element aperturaHtml = camposFilaHtml.get(COLUMNA_APERTURA);
    BigDecimal apertura = parseDecimal(aperturaHtml);

    Element cierreHtml = camposFilaHtml.get(COLUMNA_CIERRE);
    BigDecimal cierre = parseDecimal(cierreHtml);

    Element variacionHtml = camposFilaHtml.get(COLUMNA_VARIACION);
    BigDecimal variacion = parseDecimal(variacionHtml);

    Element variacionPorcentualHtml = camposFilaHtml.get(COLUMNA_VARIACION_PORCENTUAL);
    BigDecimal variacionPorcentual = parseDecimal(variacionPorcentualHtml);

    Element minimoHtml = camposFilaHtml.get(COLUMNA_MINIMO);
    BigDecimal minimo = parseDecimal(minimoHtml);

    Element maximoHtml = camposFilaHtml.get(COLUMNA_MAXIMO);
    BigDecimal maximo = parseDecimal(maximoHtml);

    Element volumenHtml = camposFilaHtml.get(COLUMNA_VOLUMEN);
    Long volumen = null;

    if (!esCampoVacio(volumenHtml.text())) {
      String volumeFiltrado = filtrarSeparadorMiles(volumenHtml.text());
      volumen = Long.parseLong(volumeFiltrado);
    }

    return Optional.of(new InformacionAdvfnDia(fecha, apertura, cierre, variacion, variacionPorcentual, minimo, maximo, volumen));
  }

  private BigDecimal parseDecimal(Element elementoDecimalHtml) {

    BigDecimal valorDecimal = null;

    if (!esCampoVacio(elementoDecimalHtml.text())) {

      String decimalFiltrado = filtrarSeparadorMiles(elementoDecimalHtml.text());
      decimalFiltrado = filtrarPorCiento(decimalFiltrado);

      valorDecimal = new BigDecimal(decimalFiltrado);
    }

    return valorDecimal;
  }

  private boolean esCampoVacio(String valorCampo) {
    return CAMPO_VACIO.equals(valorCampo);
  }

  private String filtrarSeparadorMiles(String valorOriginal) {

    return valorOriginal.replace(",", "");
  }

  private String filtrarPorCiento(String valorOriginal) {

    return valorOriginal.replace("%", "");
  }

}
