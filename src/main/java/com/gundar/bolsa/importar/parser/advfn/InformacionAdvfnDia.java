package com.gundar.bolsa.importar.parser.advfn;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.gundar.bolsa.importar.parser.Csvable;

class InformacionAdvfnDia implements Csvable {

  private LocalDate fecha;
  private BigDecimal apertura;
  private BigDecimal cierre;
  private BigDecimal variacion;
  private BigDecimal variacionPorcentual;
  private BigDecimal minimo;
  private BigDecimal maximo;
  private Long volumen;

  public InformacionAdvfnDia(LocalDate fecha, BigDecimal apertura, BigDecimal cierre, BigDecimal variacion,
      BigDecimal variacionPorcentual, BigDecimal minimo, BigDecimal maximo, Long volumen) {
    super();
    this.fecha = fecha;
    this.apertura = apertura;
    this.cierre = cierre;
    this.variacion = variacion;
    this.variacionPorcentual = variacionPorcentual;
    this.minimo = minimo;
    this.maximo = maximo;
    this.volumen = volumen;
  }

  @Override
  public String toLineaCsv() {

    return fecha.format(DateTimeFormatter.ISO_LOCAL_DATE) + SEPARADOR_CSV
        + apertura + SEPARADOR_CSV
        + cierre + SEPARADOR_CSV
        + variacion + SEPARADOR_CSV
        + variacionPorcentual + SEPARADOR_CSV
        + minimo + SEPARADOR_CSV
        + maximo + SEPARADOR_CSV
        + volumen;
  }

  @Override
  public String toString() {
    return "InformacionAdvfnDia [fecha=" + fecha + ", apertura=" + apertura + ", cierre=" + cierre + ", variacion=" + variacion
        + ", variacionPorcentual=" + variacionPorcentual + ", minimo=" + minimo + ", maximo=" + maximo + ", volumen=" + volumen + "]";
  }

  @Override
  public LocalDate getFecha() {
    return fecha;
  }

}
