package com.gundar.bolsa.importar;

import java.io.IOException;
import java.util.List;

public interface ImportadorIndice {

  public List<InformacionDia> importarInformacionDia() throws IOException;

}
