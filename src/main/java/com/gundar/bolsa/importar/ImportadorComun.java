package com.gundar.bolsa.importar;

import java.math.BigDecimal;

public class ImportadorComun {

  private ImportadorComun() {}

  public static BigDecimal calculaDiferenciaLineal(BigDecimal precioActual, BigDecimal precioAnterior) {

    return precioActual.subtract(precioAnterior);
  }

  public static double calculaDiferenciaPorcentual(double diferenciaLineal, double precioAnterior) {

    return diferenciaLineal / precioAnterior;
  }

}
