package com.gundar.bolsa.importar;

import java.math.BigDecimal;
import java.time.LocalDate;

public class InformacionDia {

  private LocalDate fecha;
  private BigDecimal precioCierre;
  private BigDecimal precioApertura;
  private double diferenciaPorcentual;
  private BigDecimal diferenciaLineal;

  public InformacionDia(LocalDate fecha, BigDecimal precioCierre, BigDecimal precioApertura, double diferenciaPorcentual,
      BigDecimal diferenciaLineal) {
    super();
    this.fecha = fecha;
    this.precioCierre = precioCierre;
    this.precioApertura = precioApertura;
    this.diferenciaPorcentual = diferenciaPorcentual;
    this.diferenciaLineal = diferenciaLineal;
  }

  @Override
  public String toString() {
    return "InformacionDia [fecha=" + fecha + ", precioCierre=" + precioCierre + ", precioApertura=" + precioApertura + ", diferenciaPorcentual="
        + diferenciaPorcentual + ", diferenciaLineal=" + diferenciaLineal + "]";
  }

  public LocalDate getFecha() {
    return fecha;
  }

  public BigDecimal getPrecioCierre() {
    return precioCierre;
  }

  public double getDiferenciaPorcentual() {
    return diferenciaPorcentual;
  }

  public BigDecimal getDiferenciaLineal() {
    return diferenciaLineal;
  }

  public BigDecimal getPrecioApertura() {
    return precioApertura;
  }

}
