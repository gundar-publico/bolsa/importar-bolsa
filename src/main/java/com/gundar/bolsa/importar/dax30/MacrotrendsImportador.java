package com.gundar.bolsa.importar.dax30;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;

class MacrotrendsImportador extends AbstractImportadorCsv<FuenteDatosDax30> {

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_VALUE = 1;

  public MacrotrendsImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new MacrotrendsImportador(Dax30Importador.RUTA_BASE_DAX30).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Collections.singletonList("macrotrends.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ISO_LOCAL_DATE;

  @Override
  protected FuenteDatosDax30 convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    LocalDate date = LocalDate.parse(dateCsv, FORMATO_FECHA);

    String valueCsv = filtrarString(record.get(CABECERA_VALUE));

    final FuenteDatosDax30 fuenteDatosDia;

    if (valueCsv == null) {

      fuenteDatosDia = new FuenteDatosDax30(date);

    } else {

      BigDecimal valorCierre = new BigDecimal(valueCsv);

      final Long volume = null;

      fuenteDatosDia = new FuenteDatosDax30(date, valorCierre, volume, "Macrotrends");
    }

    return fuenteDatosDia;
  }

}
