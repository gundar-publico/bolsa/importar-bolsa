package com.gundar.bolsa.importar.dax30.preprocesor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.dax30.Dax30Importador;
import com.gundar.bolsa.importar.parser.Csvable;
import com.gundar.bolsa.importar.parser.ImportadorHttpAbstract;
import com.gundar.bolsa.importar.parser.advfn.AdvfnParser;

class AdvfnDax30 extends ImportadorHttpAbstract {

  private static final Logger log = LoggerFactory.getLogger(AdvfnDax30.class);

  public static final LocalDate FECHA_INICIAL_INDICE = LocalDate.of(1970, 1, 1);
  private static final String URL = "https://mx.advfn.com/bolsa-de-valores/DBI/DAX/historico/more-datos-historicos";

  public AdvfnDax30() {
    super(new AdvfnParser(URL));
  }

  public static void main(String[] args) throws Exception {

    LocalDate fechaFinal = LocalDate.of(2017, 12, 31);

    new AdvfnDax30().importarDatosParaArchivo(FECHA_INICIAL_INDICE, fechaFinal);
  }

  @Override
  protected void escribirResultados(List<String> lineasTransformadas) {

    try {

      String rutaArchivo = Dax30Importador.RUTA_BASE_DAX30 + "advfn.csv";

      String cabeceras = "fecha" + Csvable.SEPARADOR_CSV
          + "apertura" + Csvable.SEPARADOR_CSV
          + "cierre" + Csvable.SEPARADOR_CSV
          + "variacion" + Csvable.SEPARADOR_CSV
          + "variacionPorcentual" + Csvable.SEPARADOR_CSV
          + "minimo" + Csvable.SEPARADOR_CSV
          + "maximo" + Csvable.SEPARADOR_CSV
          + "volumen";

      lineasTransformadas.add(0, cabeceras);

      Files.write(Paths.get(rutaArchivo), lineasTransformadas, StandardOpenOption.CREATE, StandardOpenOption.WRITE);

      if (log.isInfoEnabled()) {
        log.info("Creado archivo = [" + rutaArchivo + "]");
      }

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
