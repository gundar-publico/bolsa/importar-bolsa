package com.gundar.bolsa.importar.dax30;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.datos.CalculoDesviacion;
import com.gundar.bolsa.importar.datos.DesviacionPorcentualMaxBigDecimal;
import com.gundar.bolsa.importar.datos.DesviacionPorcentualMaxLong;
import com.gundar.bolsa.importar.datos.ExtractorPrecioCierre;
import com.gundar.bolsa.importar.datos.ExtractorVolumen;

public class Dax30Comparator extends Dax30Importador {

  private static final Logger log = LoggerFactory.getLogger(Dax30Comparator.class);

  public static void main(String[] args) throws Exception {

    new Dax30Comparator().compararPrecios();
    // new Dax30Comparator().compararVolumen();
  }

  public void compararPrecios() throws IOException {

    Map<LocalDate, Collection<FuenteDatosDax30>> fuentesAgrupadas = cargarAgruparPorDias();

    CalculoDesviacion desviacion = new DesviacionPorcentualMaxBigDecimal(1, ExtractorPrecioCierre.INSTANCIA);
    // CalculoDesviacion desviacion = new DesviacionLinealMinBigDecimal(0, ExtractorPrecioCierre.INSTANCIA);

    int distintos = 0;
    for (Map.Entry<LocalDate, Collection<FuenteDatosDax30>> diaConFuente : fuentesAgrupadas.entrySet()) {

      boolean desviado = desviacion.valoresDesviados(diaConFuente.getValue());
      if (desviado) {
        distintos++;
      }

    }

    if (log.isWarnEnabled()) {
      log.warn("Total errores = [" + distintos + "]");
    }
  }

  public void compararVolumen() throws IOException {

    Map<LocalDate, Collection<FuenteDatosDax30>> fuentesAgrupadas = cargarAgruparPorDias();

    // CalculoDesviacion desviacion = new DesviacionPorcentualMin(1, ExtractorVolumen.INSTANCIA);
    CalculoDesviacion desviacion = new DesviacionPorcentualMaxLong(1, ExtractorVolumen.INSTANCIA);
    // CalculoDesviacion desviacion = new DesviacionLinealMax(1);
    // CalculoDesviacion desviacion = new DesviacionLinealMin(5);

    int distintos = 0;
    for (Map.Entry<LocalDate, Collection<FuenteDatosDax30>> diaConFuente : fuentesAgrupadas.entrySet()) {

      boolean desviado = desviacion.valoresDesviados(diaConFuente.getValue());
      if (desviado) {
        distintos++;
      }

    }

    if (log.isWarnEnabled()) {
      log.warn("Total errores = [" + distintos + "]");
    }
  }

}
