package com.gundar.bolsa.importar.dax30;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;

class AdvfnImportador extends AbstractImportadorCsv<FuenteDatosDax30> {

  private static final int CABECERA_FECHA = 0;
  private static final int CABECERA_APERTURA = 1;
  private static final int CABECERA_CIERRE = 2;
  private static final int CABECERA_VARIACION = 3;
  private static final int CABECERA_VARIACION_PORCENTUAL = 4;
  private static final int CABECERA_MINIMO = 5;
  private static final int CABECERA_MAXIMO = 6;
  private static final int CABECERA_VOLUMEN = 7;

  public AdvfnImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new AdvfnImportador(Dax30Importador.RUTA_BASE_DAX30).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Collections.singletonList("advfn.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ISO_LOCAL_DATE;

  @Override
  protected FuenteDatosDax30 convertirRegistroCsv(CSVRecord record) {

    String fechaCsv = filtrarString(record.get(CABECERA_FECHA));
    LocalDate fecha = LocalDate.parse(fechaCsv, FORMATO_FECHA);

    String aperturaCsv = filtrarString(record.get(CABECERA_APERTURA));

    String cierreCsv = filtrarString(record.get(CABECERA_CIERRE));

    String variacionCsv = filtrarString(record.get(CABECERA_VARIACION));

    String variacionPorcentualCsv = filtrarString(record.get(CABECERA_VARIACION_PORCENTUAL));

    String minimoCsv = filtrarString(record.get(CABECERA_MINIMO));

    String maximoCsv = filtrarString(record.get(CABECERA_MAXIMO));

    String volumenCsv = filtrarString(record.get(CABECERA_VOLUMEN));

    final FuenteDatosDax30 fuenteDatosDia;

    if (cierreCsv == null) {

      if ((aperturaCsv != null) || (variacionPorcentualCsv != null) || (variacionCsv != null) || (minimoCsv != null) || (maximoCsv != null)
          || (volumenCsv != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDatosDia = new FuenteDatosDax30(fecha);

    } else {

      BigDecimal valorCierre = new BigDecimal(cierreCsv);

      Long volume = null;
      if ((volumenCsv != null) && !"0".equals(volumenCsv)) {
        volume = Long.valueOf(volumenCsv);
      }

      fuenteDatosDia = new FuenteDatosDax30(fecha, valorCierre, volume, "Advfn");
    }

    return fuenteDatosDia;
  }

}
