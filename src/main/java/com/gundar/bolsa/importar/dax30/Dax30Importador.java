package com.gundar.bolsa.importar.dax30;

import static com.gundar.bolsa.importar.ImportadorComun.calculaDiferenciaLineal;
import static com.gundar.bolsa.importar.ImportadorComun.calculaDiferenciaPorcentual;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gundar.bolsa.importar.ImportadorIndice;
import com.gundar.bolsa.importar.InformacionDia;
import com.gundar.bolsa.importar.datos.ExtractorPrecioCierre;
import com.gundar.bolsa.importar.datos.SeleccionadorPrecio;

public class Dax30Importador implements ImportadorIndice {

  private static final Logger log = LoggerFactory.getLogger(Dax30Importador.class);

  public static final String RUTA_BASE_DAX30 = "/home/gun/workspace_oxygen/importar-bolsa/src/main/resources/fuentes/dax30/";
  protected static final LocalDate FECHA_MAXIMA = LocalDate.of(2017, 12, 31);

  public static void main(String[] args) throws Exception {

    new Dax30Importador().importarInformacionDia();
  }

  @Override
  public List<InformacionDia> importarInformacionDia() throws IOException {

    Map<LocalDate, Collection<FuenteDatosDax30>> fuentesAgrupadas = cargarAgruparPorDias();

    // ConversorInformacion conversor = new ConversorInformacion(new SeleccionadorPrecio(0.75));

    SeleccionadorPrecio seleccionadorPrecioCierre = new SeleccionadorPrecio(0.75, ExtractorPrecioCierre.INSTANCIA);

    List<InformacionDia> informacionTotal = new ArrayList<>();

    // long precioAnterior = ConversorInformacion.PRECIO_VACIO;
    BigDecimal precioCierreAnterior = null;

    for (Map.Entry<LocalDate, Collection<FuenteDatosDax30>> diaConFuentes : fuentesAgrupadas.entrySet()) {

      LocalDate fecha = diaConFuentes.getKey();

      if (fecha.compareTo(FECHA_MAXIMA) > 0) {

        if (log.isDebugEnabled()) {

          log.debug("La fecha = [" + fecha + "] supera la fecha máxima = [" + FECHA_MAXIMA + "]");
        }

        continue;
      }

      BigDecimal precioCierre = seleccionadorPrecioCierre.seleccionar(diaConFuentes.getValue());

      final double diferenciaPorcentual;
      final BigDecimal diferenciaLineal;

      if (precioCierreAnterior == null) {
        diferenciaLineal = BigDecimal.ZERO;
        diferenciaPorcentual = 0;
      } else {
        diferenciaLineal = calculaDiferenciaLineal(precioCierre, precioCierreAnterior);
        diferenciaPorcentual = calculaDiferenciaPorcentual(diferenciaLineal.doubleValue(), precioCierreAnterior.doubleValue());
      }

      final BigDecimal precioApertura = null;
      // InformacionDia informacionDia = conversor.convertir(precioCierreAnterior, diaConFuentes.getValue());
      InformacionDia informacionDia = new InformacionDia(fecha, precioCierre, precioApertura, diferenciaPorcentual, diferenciaLineal);
      informacionTotal.add(informacionDia);

      precioCierreAnterior = informacionDia.getPrecioCierre();
    }

    return informacionTotal;
  }

  protected Map<LocalDate, Collection<FuenteDatosDax30>> cargarAgruparPorDias() throws IOException {

    // List<FuenteDatosDax30> valoresGoogle = new GoogleImportador(RUTA_BASE_DAX30).importar();

    List<FuenteDatosDax30> valoresElMundo = new ElMundoImportador(RUTA_BASE_DAX30).importar();

    List<FuenteDatosDax30> valoresAdvfn = new AdvfnImportador(RUTA_BASE_DAX30).importar();

    List<FuenteDatosDax30> valoresMacrotrends = new MacrotrendsImportador(RUTA_BASE_DAX30).importar();

    List<FuenteDatosDax30> valoresStooq = new StooqImportador(RUTA_BASE_DAX30).importar();

    List<FuenteDatosDax30> valoresWallStreet = new WallStreetImportador(RUTA_BASE_DAX30).importar();

    List<FuenteDatosDax30> valoresYahoo = new YahooImportador(RUTA_BASE_DAX30).importar();

    Map<LocalDate, Collection<FuenteDatosDax30>> fuentesAgrupadas = agrupar(
        valoresElMundo,
        valoresAdvfn,
        valoresMacrotrends,
        valoresWallStreet,
        valoresYahoo,
        // valoresGoogle,
        valoresStooq);

    return fuentesAgrupadas;
  }

  @SafeVarargs
  private final Map<LocalDate, Collection<FuenteDatosDax30>> agrupar(Collection<FuenteDatosDax30>... todosLosValores) {

    Map<LocalDate, Collection<FuenteDatosDax30>> informacionPorDia = new TreeMap<>();

    for (Collection<FuenteDatosDax30> valores : todosLosValores) {

      for (FuenteDatosDax30 valorDia : valores) {

        Collection<FuenteDatosDax30> valoresAgrupados = informacionPorDia.get(valorDia.getFecha());

        if (valoresAgrupados == null) {
          valoresAgrupados = new ArrayList<>(todosLosValores.length);
          informacionPorDia.put(valorDia.getFecha(), valoresAgrupados);
        }

        valoresAgrupados.add(valorDia);
      }
    }

    return informacionPorDia;
  }

}
