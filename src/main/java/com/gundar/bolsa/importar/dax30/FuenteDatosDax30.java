package com.gundar.bolsa.importar.dax30;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import com.gundar.bolsa.importar.FuenteDatosDia;

class FuenteDatosDax30 implements FuenteDatosDia {

  private static final int ESCALA_PRECIO = 2;
  private static final RoundingMode REDONDEO_PRECIO = RoundingMode.HALF_DOWN;

  private LocalDate fecha;
  private BigDecimal precioCierre;
  private Long volumen;
  private String origen;

  public FuenteDatosDax30(LocalDate fecha) {
    this(fecha, null, null, null);
  }

  public FuenteDatosDax30(LocalDate fecha, BigDecimal precioCierre, Long volumen, String origen) {
    super();
    this.fecha = fecha;
    this.precioCierre = redondear(precioCierre);
    this.volumen = volumen;
    this.origen = origen;
  }

  private final BigDecimal redondear(BigDecimal valor) {

    BigDecimal valorRedondeado = null;

    if (valor != null) {
      valorRedondeado = valor.setScale(ESCALA_PRECIO, REDONDEO_PRECIO);
    }

    return valorRedondeado;
  }

  @Override
  public boolean esVacio() {
    return precioCierre == null;
  }

  @Override
  public int compareTo(FuenteDatosDia otraInfo) {

    return fecha.compareTo(otraInfo.getFecha());
  }

  @Override
  public String toString() {
    return "FuenteDatosDax30 [fecha=" + fecha + ", precioCierre=" + precioCierre + ", volumen=" + volumen + ", origen=" + origen + "]";
  }

  @Override
  public LocalDate getFecha() {
    return fecha;
  }

  @Override
  public Long getVolumen() {
    return volumen;
  }

  @Override
  public String getOrigen() {
    return origen;
  }

  @Override
  public BigDecimal getPrecioCierre() {
    return precioCierre;
  }

  @Override
  public BigDecimal getPrecioApertura() {
    return null;
  }

}
