package com.gundar.bolsa.importar.dax30;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

class AdvfnCorregirDias {

  public static void main(String[] args) throws Exception {

    new AdvfnCorregirDias().a();
  }

  public void a() throws Exception {

    List<FuenteDatosDax30> valoresStooq = new StooqImportador(Dax30Importador.RUTA_BASE_DAX30).importar();

    Path rutaArchivo = Paths.get("/home/gun/workspace_oxygen/importar-bolsa/src/main/resources/fuentes/dax30/advfn_2.csv");

    CSVPrinter printer = CSVFormat.DEFAULT
        .withHeader("fecha", "apertura", "cierre", "variacion", "variacionPorcentual", "minimo", "maximo", "volumen")
        .print(rutaArchivo, StandardCharsets.UTF_8);

    Iterable<CSVRecord> registros = cargarRegistros();

    CSVRecord registroAnterior = null;

    final LocalDate fechaLimite = LocalDate.of(2007, 07, 05);

    for (CSVRecord registro : registros) {

      if (registroAnterior == null) {
        registroAnterior = registro;
        continue;
      }

      String fechaString = registro.get(0);
      LocalDate fecha = LocalDate.parse(fechaString);

      if (fechaLimite.compareTo(fecha) == 0) {
        System.out.println();
      }

      String cierreString = registro.get(2);
      BigDecimal cierre = new BigDecimal(cierreString);
      cierre = cierre.setScale(0, RoundingMode.HALF_DOWN);

      FuenteDatosDax30 referencia = buscarDia(fecha, valoresStooq);

      if (referencia != null) {

        if (cierre.compareTo(referencia.getPrecioCierre().setScale(0, RoundingMode.HALF_DOWN)) == 0) {

          printer.printRecord(
              registro.get(0),
              registro.get(1),
              registro.get(2),
              registro.get(3),
              registro.get(4),
              registro.get(5),
              registro.get(6),
              registro.get(7));

        } else {

          printer.printRecord(
              registro.get(0),
              registroAnterior.get(1),
              registroAnterior.get(2),
              registroAnterior.get(3),
              registroAnterior.get(4),
              registroAnterior.get(5),
              registroAnterior.get(6),
              registroAnterior.get(7));
        }
      } else {

        printer.printRecord(
            registro.get(0),
            registro.get(1),
            registro.get(2),
            registro.get(3),
            registro.get(4),
            registro.get(5),
            registro.get(6),
            registro.get(7));
      }

      registroAnterior = registro;
    }

    printer.flush();

  }

  private FuenteDatosDax30 buscarDia(LocalDate fecha, List<FuenteDatosDax30> referencias) {

    for (FuenteDatosDax30 referencia : referencias) {

      if (referencia.getFecha().compareTo(fecha) == 0) {
        return referencia;
      }
    }

    return null;
  }

  private Iterable<CSVRecord> cargarRegistros() {

    try {

      String rutaArchivo = "/home/gun/workspace_oxygen/importar-bolsa/src/main/resources/fuentes/dax30/advfn.csv";

      Reader in = new FileReader(rutaArchivo);

      CSVFormat parserCsv = CSVFormat.DEFAULT;

      if (true) {
        parserCsv = parserCsv.withFirstRecordAsHeader();
      }

      Iterable<CSVRecord> records = parserCsv.parse(in);

      return records;

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
