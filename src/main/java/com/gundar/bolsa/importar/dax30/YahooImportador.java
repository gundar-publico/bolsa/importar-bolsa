package com.gundar.bolsa.importar.dax30;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.csv.CSVRecord;

import com.gundar.bolsa.importar.AbstractImportadorCsv;

class YahooImportador extends AbstractImportadorCsv<FuenteDatosDax30> {

  private static final int CABECERA_DATE = 0;
  private static final int CABECERA_OPEN = 1;
  private static final int CABECERA_HIGH = 2;
  private static final int CABECERA_LOW = 3;
  private static final int CABECERA_CLOSE = 4;
  private static final int CABECERA_ADJ_CLOSE = 5;
  private static final int CABECERA_VOLUME = 6;

  public YahooImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos, true);
  }

  public static void main(String[] args) throws Exception {

    new YahooImportador(Dax30Importador.RUTA_BASE_DAX30).importar();
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    return Collections.singletonList("yahoo.csv");
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ISO_LOCAL_DATE;

  @Override
  protected FuenteDatosDax30 convertirRegistroCsv(CSVRecord record) {

    String dateCsv = filtrarString(record.get(CABECERA_DATE));
    LocalDate fecha = LocalDate.parse(dateCsv, FORMATO_FECHA);

    String openCsv = filtrarString(record.get(CABECERA_OPEN));

    String highCsv = filtrarString(record.get(CABECERA_HIGH));

    String lowCsv = filtrarString(record.get(CABECERA_LOW));

    String closeCsv = filtrarString(record.get(CABECERA_CLOSE));

    String adjCloseCsv = filtrarString(record.get(CABECERA_ADJ_CLOSE));

    String volumeCsv = filtrarString(record.get(CABECERA_VOLUME));

    final FuenteDatosDax30 fuenteDatosDia;

    if (closeCsv == null) {

      if ((openCsv != null) || (highCsv != null) || (lowCsv != null) || (adjCloseCsv != null) || (volumeCsv != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDatosDia = new FuenteDatosDax30(fecha);

    } else {

      BigDecimal valorCierre = new BigDecimal(closeCsv);

      Long volume = null;
      if ((volumeCsv != null) && !"0".equals(volumeCsv)) {
        volume = Long.valueOf(volumeCsv);
      }

      fuenteDatosDia = new FuenteDatosDax30(fecha, valorCierre, volume, "Yahoo");
    }

    return fuenteDatosDia;
  }

}
