package com.gundar.bolsa.importar.dax30;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.gundar.bolsa.importar.AbstractImportadorJson;


class ElMundoImportador extends AbstractImportadorJson<FuenteDatosDax30> {

  private static final String PRECIO = "precio";
  private static final String PRECIO_ANTERIOR = "precio_anterior";
  private static final String VOLUMEN = "volumen";
  private static final String EFECTIVO = "efectivo";
  private static final String MAXIMO = "maximo";
  private static final String MINIMO = "minimo";
  private static final String DIFERENCIA = "diferencia";
  private static final String RENTABILIDAD = "rentabilidad";
  private static final String PRECIO_APERTURA = "precio_apertura";
  private static final String PRECIO_MEDIO = "precio_medio";
  private static final String HORA = "hora";
  private static final String FECHA = "fecha";

  public ElMundoImportador(String rutaBaseArchivos) {
    super(rutaBaseArchivos);
  }

  public static void main(String[] args) throws Exception {

    new ElMundoImportador(Dax30Importador.RUTA_BASE_DAX30).importar();
  }

  @Override
  protected Iterable<JsonNode> cargarRegistrosJson(String jsonString) throws IOException {

    JsonNode raiz = jsonMapper.readTree(jsonString);

    JsonNode cotizaciones = raiz.get("valor").get("cotizaciones");

    return cotizaciones;
  }

  @Override
  protected Collection<String> getNombresArchivos() {

    String rutaBase = "el_mundo/anyo.txt";

    final int anyoInicial = 2000;
    final int anyoFinal = 2017;

    List<String> rutasArchivos = new ArrayList<>();

    for (int anyoActual = anyoInicial; anyoActual <= anyoFinal; anyoActual++) {

      String rutaArchivo = rutaBase.replace("anyo", String.valueOf(anyoActual));

      rutasArchivos.add(rutaArchivo);
    }

    return rutasArchivos;
  }

  private static final DateTimeFormatter FORMATO_FECHA = DateTimeFormatter.ofPattern("dd/MM/yyyy");

  @Override
  protected FuenteDatosDax30 convertirRegistro(JsonNode registro) {


    String precioString = extraerString(registro, PRECIO);

    String precioAnteriorString = extraerString(registro, PRECIO_ANTERIOR);

    String volumenString = extraerString(registro, VOLUMEN);

    String efectivoString = extraerString(registro, EFECTIVO);

    String maximoString = extraerString(registro, MAXIMO);

    String minimoString = extraerString(registro, MINIMO);

    String diferenciaString = extraerString(registro, DIFERENCIA);

    String rentabilidadString = extraerString(registro, RENTABILIDAD);

    String precioAperturaString = extraerString(registro, PRECIO_APERTURA);

    String precioMedioString = extraerString(registro, PRECIO_MEDIO);

    String horaString = extraerString(registro, HORA);

    String fechaString = extraerString(registro, FECHA);
    LocalDate fecha = LocalDate.parse(fechaString, FORMATO_FECHA);

    final FuenteDatosDax30 fuenteDia;

    if (precioString == null) {

      if ((precioAnteriorString != null) || (volumenString != null) || (efectivoString != null) || (maximoString != null)
          || (minimoString != null) || (diferenciaString != null) || (rentabilidadString != null) || (precioAperturaString != null)
          || (precioMedioString != null) || (horaString != null)) {

        throw new IllegalStateException("Fecha = [" + fecha + "] rara");
      }

      fuenteDia = new FuenteDatosDax30(fecha);

    } else {

      BigDecimal precio = new BigDecimal(precioString);

      Long volumen = null;
      // if (volumenString != null) {
      // volumen = Long.valueOf(volumenString);
      // }

      fuenteDia = new FuenteDatosDax30(fecha, precio, volumen, "El Mundo");
    }

    return fuenteDia;

  }

  private String filtrarCaracteres(String valor) {

    return valor.replace(".", "").replace(",", ".");
  }

  private String extraerString(JsonNode registro, String campo) {

    JsonNode valorJn = registro.get(campo);

    if (!valorJn.isValueNode()) {
      throw new IllegalStateException("El campo [" + campo + "] no es un nodo final = [" + valorJn + "]");
    }

    String valor = valorJn.asText();

    valor = filtrarString(valor);

    if (valor != null) {

      valor = filtrarCaracteres(valor);
    }

    return valor;
  }

}
